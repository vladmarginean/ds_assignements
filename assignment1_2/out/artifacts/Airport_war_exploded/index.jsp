<%@ page import="com.sun.org.apache.xpath.internal.operations.Bool" %><%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 05.11.2016
  Time: 13:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/myStyle.css">
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery-3.1.1.js"></script>
    <title>Login</title>
</head>
<body>
    <div class="container">
        <form action="/login" method="post" id="loginForm" role="form" >
            <h2>Login</h2>
            <div class="form-group col-xs-12">
                <input type="text" name="username" id="username" class="form-control" required="true"
                       autocomplete="false" placeholder="Username..."/>
            </div>
            <div class="form-group col-xs-12">
                <input type="password" name="password" id="password" class="form-control" required="true"
                       autocomplete="false" placeholder="Password..."/>
            </div>
            <button type="submit" class="btn btn-info">
                <span class="glyphicon glyphicon-log-in"></span> Login
            </button>
            <%
                if (request != null) {
                    Boolean invalidCredentials = (Boolean)request.getAttribute("invalidCredentials");
                    if (invalidCredentials != null && invalidCredentials) {
                    %>
                    <label class="error-label">Invalid credentials</label>
                    <%
                    }
                }
            %>
            <br/>
            <br/>
        </form>
    </div>
</body>