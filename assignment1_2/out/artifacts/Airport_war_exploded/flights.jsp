<%@ page import="DataAccess.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="BusinessLayer.LoginServlet" %>
<%@ page import="com.sun.org.apache.xpath.internal.operations.Bool" %><%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 05.11.2016
  Time: 14:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="../css/myStyle.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
    <script src="../js/bootstrap-datetimepicker.js"></script>
    <title>Flights</title>
</head>
<body>
    <div class="container">
        <h2>Airport flights</h2>
        <button type="button" class="btn btn-info"
                style="float: right;"
                onclick="location.href='/login'">
            <span class="glyphicon glyphicon-log-out"></span> Log out
        </button>
        <div id="flightsTable">
            <table class="table table-hover">
                <thead style="background-color: #265a88; color:white">
                    <tr>
                        <th>Flight ID</th>
                        <th>Flight number</th>
                        <th>Airplane type</th>
                        <th>Departure</th>
                        <th>Time</th>
                        <th>Arrival</th>
                        <th>Time</th>
                        <%
                            Integer userAccess = (Integer)request.getAttribute("access");
                            if (userAccess != null && userAccess == 1) {
                        %>
                        <th></th>
                        <%}%>
                    </tr>
                </thead>
                <tbody>
                    <%
                        ArrayList<Flight> flights = (ArrayList)request.getAttribute("flights");
                        if (flights != null) {
                            Iterator it = flights.iterator();
                            while(it.hasNext())
                            {
                                Flight f= (Flight)it.next();
                                %>
                                <tr id="flightRow">
                                    <td><%=f.getFlightId()%></td>
                                    <td><%=f.getFlightNumber()%></td>
                                    <td><%=f.getAirplaneType()%></td>
                                    <td><%=f.getDepartureCity().getName()%></td>
                                    <td class="myTooltip">
                                        <%=f.getDeparture()%>
                                        <span class="tooltiptext">Transformed departure</span>
                                    </td>
                                    <td><%=f.getArrivalCity().getName()%></td>
                                    <td class="myTooltip">
                                        <%=f.getArrival()%>
                                        <span class="tooltiptext">Transformed arrival</span>
                                    </td>
                                    <%
                                        if (userAccess != null && userAccess == 1) {
                                        %>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-warning" onclick="updateFlight(<%=f.getFlightId()%>)">Edit</button>
                                            <button type="button" class="btn btn-sm btn-danger" onclick="deleteFlight(<%=f.getFlightId()%>)">Delete</button>
                                        </td>
                                        <%
                                        }
                                    %>
                                </tr>
                                <%
                            }
                        }
                    %>
                </tbody>
            </table>
        </div>
        <label class="text small" style="margin-top: -10px;">*Hover on departure/arrival times to see the local timezones</label>
        <%
            if (userAccess == 1) {
            Flight fl = (Flight)request.getAttribute("selectedFlight");
        %>
            <form action="/flights" method="post" id="editForm" role="form" class="form form-group" style="width: 40%">
                <br/><label>Insert/Edit flights form</label>
                <input type="text" id="flightId" name="flightId" class="form-control" value="<%=fl.getFlightId()%>"><br/>
                <input type="text" id="flightNumber" class="form-control" placeholder="Flight number"
                       name="flightNumber" value="<%=fl.getFlightNumber()%>"><br/>
                <input type="text" id="airplaneType" class="form-control" placeholder="Airplane type"
                       name="airplaneType" value="<%=fl.getAirplaneType()%>"><br/>
                <select id="departureCity" class="form-control"
                        name="departureCity" value="<% if (fl.getDepartureCity() != null) fl.getDepartureCity().getCityId();%>">
                    <%
                        ArrayList<City> cities = (ArrayList)request.getAttribute("cities");
                        if (cities != null) {
                            Iterator it = cities.iterator();
                            while (it.hasNext()) {
                                City c = (City) it.next();
                    %>
                    <option value=<%=c.getCityId()%>><%=c.getName()%></option>
                    <%
                            }
                        }
                    %>
                </select><br/>
                <div class="form-group">
                    <div class='input-group date' id='departureTimePicker'>
                        <input type='text' class="form-control"
                               name="departure" value="<%=fl.getDeparture()%>"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>
                <select id="arrivalCity" class="form-control"
                        name="arrivalCity" value="<% if(fl.getArrivalCity() != null) fl.getArrivalCity().getCityId();%>">
                    <%
                        if (cities != null) {
                            Iterator it = cities.iterator();
                            while (it.hasNext()) {
                                City c = (City) it.next();
                    %>
                                <option value="<%=c.getCityId()%>"><%=c.getName()%></option>
                    <%
                            }
                        }
                    %>
                </select><br/>
                <div class="form-group">
                    <div class='input-group date' id='arrivalTimePicker'>
                        <input type='text' class="form-control"
                               name="arrival" value="<%=fl.getArrival()%>"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>
                <button type="submit" class="btn btn-info">
                    <span class="glyphicon glyphicon-save-file"></span> Save
                </button>
                <br/>
                <br/>
            </form>
        <%}%>
    </div>
</body>

<script type="text/javascript">
    $(function () {
        $('#departureTimePicker').datetimepicker();
        $('#arrivalTimePicker').datetimepicker();
    });

    function deleteFlight(flightId) {
        $.ajax({
            url: 'http://localhost:8080/flights',
            type: 'DELETE',
            headers: {
                flightId: flightId
            },
            data: {
                flightId: flightId
            }
        });
        location.reload();
    };

    function updateFlight(flightId) {
        $.ajax({
            url: 'http://localhost:8080/flights',
            type: 'GET',
            headers: {
                flightId: flightId
            },
            data: {
                flightId: flightId
            }
        });
    };
</script>
