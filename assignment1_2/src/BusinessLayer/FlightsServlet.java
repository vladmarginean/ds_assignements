package BusinessLayer;

import DataAccess.City;
import DataAccess.CityDAO;
import DataAccess.Flight;
import DataAccess.FlightDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Vlad on 30.10.2016.
 */
@WebServlet(name = "/flightsServlet", urlPatterns = {"/flights"})
public class FlightsServlet extends HttpServlet{
    private FlightDAO flightDao;
    private CityDAO cityDao;

    public FlightsServlet() {
        flightDao = new FlightDAO();
        cityDao = new CityDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight = new Flight();
        flight.setAirplaneType(request.getParameter("airplaneType"));
        flight.setFlightNumber(request.getParameter("flightNumber"));
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
        int departureCityId = Integer.parseInt(request.getParameter("departureCity"));
        flight.setDepartureCity(cityDao.getCity(departureCityId));
        try {
            Date departure = format.parse(request.getParameter("departure"));
            flight.setDeparture(departure);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int arrivalCityId = Integer.parseInt(request.getParameter("arrivalCity"));
        flight.setArrivalCity(cityDao.getCity(arrivalCityId));
        try {
            Date arrival = format.parse(request.getParameter("arrival"));
            flight.setArrival(arrival);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fId = request.getParameter("flightId");
        if (fId != null && fId != "") {
            int flightId = Integer.parseInt(fId);
            flight.setFlightId(flightId);
            if (flightId == 0) {
                flightDao.addFlight(flight);
            } else {
                flightDao.updateFlight(flight);
            }
        } else {
            flight.setFlightId(0);
            flightDao.addFlight(flight);
        }
        response.sendRedirect("/flights");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("username") != null) {
            List<Flight> flights = flightDao.getFlights();
            List<City> cities = cityDao.getCities();

            request.setAttribute("flights", flights);
            request.setAttribute("cities", cities);
            request.setAttribute("access", session.getAttribute("access"));
            String flightId = request.getHeader("flightId");
            if (flightId != null) {
                int fId = Integer.parseInt(flightId);
                request.setAttribute("selectedFlight", flightDao.getFlight(fId));
            } else {
                request.setAttribute("selectedFlight", new Flight());
            }
            RequestDispatcher rd = request.getRequestDispatcher("/flights.jsp");
            rd.forward(request, response);
        } else {
            response.sendRedirect("/");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fId = request.getHeader("flightId");
        if (fId != null) {
            int flightId = Integer.parseInt(fId);
            flightDao.deleteFlight(flightId);
            response.sendRedirect("/flights");
        }
    }
}
