package DataAccess;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Created by Vlad on 30.10.2016.
 */
public class AirportSessionFactory {
    private static SessionFactory airportSessionFactory;
    private static ServiceRegistry serviceRegistry;

    public static Session getSession() {
        if (airportSessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                configuration.configure();
                serviceRegistry = new ServiceRegistryBuilder().applySettings(
                        configuration.getProperties()).buildServiceRegistry();
                airportSessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Throwable ex) {
                throw new ExceptionInInitializerError(ex);
            }
        }
        return airportSessionFactory.openSession();
    }
}
