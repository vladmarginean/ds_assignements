package DataAccess;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 08.11.2016.
 */
public class CityDAO {
    public CityDAO() {}

    public List<City> getCities() {
        Session session = AirportSessionFactory.getSession();
        List<City> allCities = new ArrayList<>();
        try {
            allCities = session.createCriteria(City.class).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return allCities;
    }

    public City getCity(int cityId) {
        Session session = AirportSessionFactory.getSession();
        City city = null;
        try {
            Criteria criteria = session.createCriteria(City.class);
            city = (City) criteria.add(Restrictions.eq("cityId", cityId)).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return city;
    }
}
