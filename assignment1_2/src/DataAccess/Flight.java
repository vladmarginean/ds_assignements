package DataAccess;
/**
 * Created by Vlad on 29.10.2016.
 */

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "flights")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "flightId")
    private int flightId;
    @Column(name = "flightNumber")
    private String flightNumber;
    @Column(name = "airplaneType")
    private String airplaneType;
    @OneToOne()
    @JoinColumn(name = "departureCityId")
    private City departureCity;
    @OneToOne()
    @JoinColumn(name = "arrivalCityId")
    private City arrivalCity;
    @Column(name = "departure")
    private Date departure;
    @Column(name = "arrival")
    private Date arrival;

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        if (flightNumber != null) {
            this.flightNumber = flightNumber;
        } else {
            this.flightNumber = "";
        }
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        if (airplaneType != null) {
            this.airplaneType = airplaneType;
        } else {
            this.airplaneType = "";
        }
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCityId) {
        this.departureCity = departureCityId;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCityId) {
        this.arrivalCity = arrivalCityId;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightId=" + flightId +
                ", flightNumber='" + flightNumber + '\'' +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCity=" + departureCity +
                ", arrivalCity=" + arrivalCity +
                ", departure=" + departure +
                ", arrival=" + arrival +
                '}';
    }
}
