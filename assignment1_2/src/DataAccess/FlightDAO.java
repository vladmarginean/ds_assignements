package DataAccess;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 30.10.2016.
 */
public class FlightDAO {

    public FlightDAO() {}

    public List<Flight> getFlights() {
        Session session = AirportSessionFactory.getSession();
        List<Flight> allFlights = new ArrayList<Flight>();
        try {
            allFlights = session.createCriteria(Flight.class).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return allFlights;
    }

    public Flight getFlight(int flightId) {
        Session session = AirportSessionFactory.getSession();
        Flight flight = null;
        try {
            flight = (Flight) session.get(Flight.class, flightId);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flight;
    }

    public Integer addFlight(Flight flight) {
        Session session = AirportSessionFactory.getSession();
        Transaction tx = null;
        int flightId = 0;
        try {
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flightId;
    }

    public boolean updateFlight(Flight flight) {
        Session session = AirportSessionFactory.getSession();
        Transaction tx = null;
        boolean result = true;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            result = false;
        } finally {
            session.close();
        }
        return result;
    }

    public boolean deleteFlight(int flightId) {
        Session session = AirportSessionFactory.getSession();
        Transaction tx = null;
        boolean result = true;
        try {
            tx = session.beginTransaction();
            Flight flight = (Flight) session.get(Flight.class, flightId);
            if (flight != null) {
                session.delete(flight);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            result = false;
        } finally {
            session.close();
        }
        return result;
    }
}
