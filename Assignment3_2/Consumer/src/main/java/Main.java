import java.io.IOException;

/**
 * Created by Vlad on 29.11.2016.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        QueueConsumer consumer = new QueueConsumer("queue");
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();
    }
}
