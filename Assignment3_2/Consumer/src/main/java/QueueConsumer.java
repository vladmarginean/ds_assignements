import java.io.IOException;
import java.io.PrintWriter;

import Entities.Dvd;
import com.rabbitmq.client.*;
import org.apache.commons.lang.SerializationUtils;
import com.rabbitmq.client.AMQP.BasicProperties;

/**
 * Created by Vlad on 28.11.2016.
 */
public class QueueConsumer implements Runnable, Consumer {
    protected com.rabbitmq.client.Channel channel;
    protected Connection connection;
    protected String endPointName;

    public QueueConsumer(String endPointName) throws IOException {
        this.endPointName = endPointName;

        //Create a connection factory
        ConnectionFactory factory = new ConnectionFactory();

        //hostname of your rabbitmq server
        factory.setHost("localhost");

        //getting a connection
        connection = factory.newConnection();

        //creating a channel
        channel = connection.createChannel();

        //declaring a queue for this channel. If queue does not exist,
        //it will be created on the server.
        channel.queueDeclare(endPointName, false, false, false, null);
    }

    public void run() {
        try {
            //start consuming messages. Auto acknowledge messages.
            channel.basicConsume(endPointName, true, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when consumer is registered.
     */
    public void handleConsumeOk(String consumerTag) {
        System.out.println("Consumer " + consumerTag + " registered");
    }

    /**
     * Called when new message is available.
     */
    public void handleDelivery(String consumerTag, Envelope env,
                               BasicProperties props, byte[] body) throws IOException {
        Dvd dvd = null;

        try {
            dvd = new Dvd(( String) SerializationUtils.deserialize(body));
            MailService mailService = new MailService("tudor.vlad.ds@gmail.com","margeaua");
            System.out.println("Sending mail with " + dvd.getTitle());
            mailService.sendMail("vladmarginean9@gmail.com", "New DVD containing " + dvd.getTitle() + " added", dvd.toString());
            try {
                System.out.println("Writing file with " + dvd.getTitle());
                PrintWriter writer = new PrintWriter(dvd.getTitle() + ".txt", "UTF-8");
                writer.println("Dvd title: " + dvd.getTitle());
                writer.println("Year: " + dvd.getYear());
                writer.println("Price: $" + dvd.getPrice());
                writer.close();
            } catch (Exception ex) {
                System.out.println("Message has incorrect format");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleCancel(String consumerTag) {
    }

    public void handleCancelOk(String consumerTag) {
    }

    public void handleRecoverOk(String consumerTag) {
    }

    public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {
    }
}
