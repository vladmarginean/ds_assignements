package Entities;

import java.io.Serializable;

/**
 * Created by Vlad on 27.11.2016.
 */
public class Dvd implements Serializable{
    private String title;
    private transient int year;
    private transient double price;

    public Dvd() {}

    public Dvd(String message) throws Exception {
        try {
            String fields[] = message.split(",");
            this.title = fields[0].substring(fields[0].indexOf("title=") + 6);
            this.year = Integer.parseInt(fields[1].substring(fields[1].indexOf("year=") + 5));
            this.price = Double.parseDouble(fields[2].substring(fields[2].indexOf("price=") + 6));
        } catch (Exception ex) {
            throw ex;
        }
    }

    public Dvd(String title, int year, double price) {
        this.title = title;
        this.year = year;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Dvd: " +
                "title=" + title +
                ", year=" + year +
                ", price=" + price;
    }
}
