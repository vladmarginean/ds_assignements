package Servlets;

import Entities.Dvd;
import Messaging.Producer;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

/**
 * Created by Vlad on 27.11.2016.
 */
@WebServlet(name = "/dvdServlet", urlPatterns = {"/dvds"})
public class DvdServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        Dvd dvd = new Dvd();
        dvd.setTitle(request.getParameter("title"));
        Integer year;
        Double price;
        try {
            year = Integer.parseInt(request.getParameter("year"));
            price = Double.parseDouble(request.getParameter("price"));
            dvd.setYear(year);
            dvd.setPrice(price);
            // Place dvd in queue
        } catch (Exception e) {
            // Cannot process dvd
        }

        Producer producer = new Producer("queue");
        producer.sendMessage(dvd.toString());

        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
    }
}
