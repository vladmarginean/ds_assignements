<%--
  Created by IntelliJ IDEA.
  User: Vlad
  Date: 27.11.2016
  Time: 19:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="../css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../css/myStyle.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
    <script src="../js/bootstrap-datetimepicker.js"></script>
    <script src="../js/bootstrap-datepicker.js"></script>
    <title>Dvds</title>
  </head>
  <body>
    <h3>Manage DVDs</h3>
    <form action="/dvds" method="post" id="dvdForm" role="form" class="form form-group" style="width: 40%">
      <br/><label>Insert dvd</label>
      <input type="text" id="title" name="title" class="form-control" placeholder="Dvd title"><br/>
      <div class="form-group">
        <div class='input-group date' id='yearTimePicker'>
          <input type='text' class="form-control"
                 name="year" placeholder="Year"/>
          <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
      </div>
      <input type="text" id="price" class="form-control" placeholder="Dvd price"
             name="price"><br/>
      <button type="submit" class="btn btn-info">
        <span class="glyphicon glyphicon-save-file"></span> Save
      </button>
      <br/>
      <br/>
    </form>
  </body>
</html>

<script type="text/javascript">
  $(function () {
    $('#yearTimePicker').datepicker({
      format: "yyyy", // Notice the Extra space at the beginning
      viewMode: "years",
      minViewMode: "years"
    });
  });
</script>