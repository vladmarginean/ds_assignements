﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace TrackingApplication.Repository.Repo
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal TrackingApplicationContext Context;
        internal DbSet<TEntity> DbSet;

        public GenericRepository(TrackingApplicationContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return orderBy?.Invoke(query).ToList() ?? query.ToList();
        }

        public virtual TEntity GetByID(object id, bool detach = false)
        {
            var entity = DbSet.Find(id);

            if (detach)
            {
                Context.Entry(entity).State = EntityState.Detached;
            }

            return entity;
        }

        public virtual bool Insert(TEntity entity)
        {
            try
            {
                DbSet.Add(entity);
                Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public virtual bool Update(TEntity entityToUpdate)
        {
            try
            {
                DbSet.Attach(entityToUpdate);
                Context.Entry(entityToUpdate).State = EntityState.Modified;
                Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool Delete(object id)
        {
            var entityToDelete = DbSet.Find(id);
            return Delete(entityToDelete);
        }

        public virtual bool Delete(TEntity entityToDelete)
        {
            try
            {
                if (Context.Entry(entityToDelete).State == EntityState.Detached)
                {
                    DbSet.Attach(entityToDelete);
                }
                DbSet.Remove(entityToDelete);
                Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
