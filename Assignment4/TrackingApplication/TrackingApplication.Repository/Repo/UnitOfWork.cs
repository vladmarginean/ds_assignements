﻿using System;
using TrackingApplication.Repository.Models;

namespace TrackingApplication.Repository.Repo
{
    public class UnitOfWork : IDisposable
    {
        private TrackingApplicationContext _context = new TrackingApplicationContext();
        private GenericRepository<User> _userRepository;
        private GenericRepository<Package> _packageRepository;
        private GenericRepository<RouteStop> _routeStopRepository;

        public GenericRepository<User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new GenericRepository<User>(_context);
                }
                return _userRepository;
            }
        }

        public GenericRepository<Package> PackageRepository
        {
            get
            {
                if (_packageRepository == null)
                {
                    _packageRepository = new GenericRepository<Package>(_context);
                }
                return _packageRepository;
            }
        }

        public GenericRepository<RouteStop> RouteStopRepository
        {
            get
            {
                if (_routeStopRepository == null)
                {
                    _routeStopRepository = new GenericRepository<RouteStop>(_context);
                }
                return _routeStopRepository;
            }
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
