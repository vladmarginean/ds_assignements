﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TrackingApplication.Repository.Models;

namespace TrackingApplication.Repository.Repo
{
    public class TrackingApplicationContext : DbContext
    {
        public TrackingApplicationContext()
            : base("TrackingApplication")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<RouteStop> RouteStops { get; set; }
    }
}
