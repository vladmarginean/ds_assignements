﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrackingApplication.Repository.Models
{
    public class User
    {
        public int ID { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }

        public int Role { get; set; }

        [ForeignKey("SenderID")]
        public virtual List<Package> SentPackages { get; set; }
        [ForeignKey("ReceiverID")]
        public virtual List<Package> ReceivedPackages { get; set; }
    }
}
