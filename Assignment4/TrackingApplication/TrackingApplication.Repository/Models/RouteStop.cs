﻿using System;

namespace TrackingApplication.Repository.Models
{
    public class RouteStop
    {
        public int ID { get; set; }

        public string City { get; set; }
        public DateTime Time { get; set; }

        public int PackageID { get; set; }

        public virtual Package Package { get; set; }
    }
}
