﻿namespace TrackingApplication.Repository.Enums
{
    public enum PackageTracking
    {
        Untracked = 0,
        Tracked = 1
    }
}
