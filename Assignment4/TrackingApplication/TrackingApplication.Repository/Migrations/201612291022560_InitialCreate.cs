namespace TrackingApplication.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Packages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        SenderCity = c.String(),
                        DestinationCity = c.String(),
                        Tracking = c.Int(nullable: false),
                        SenderID = c.Int(nullable: false),
                        ReceiverID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.ReceiverID)
                .ForeignKey("dbo.Users", t => t.SenderID)
                .Index(t => t.SenderID)
                .Index(t => t.ReceiverID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address = c.String(),
                        City = c.String(),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RouteStops",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        City = c.String(),
                        Time = c.DateTime(nullable: false),
                        PackageID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Packages", t => t.PackageID)
                .Index(t => t.PackageID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RouteStops", "PackageID", "dbo.Packages");
            DropForeignKey("dbo.Packages", "SenderID", "dbo.Users");
            DropForeignKey("dbo.Packages", "ReceiverID", "dbo.Users");
            DropIndex("dbo.RouteStops", new[] { "PackageID" });
            DropIndex("dbo.Packages", new[] { "ReceiverID" });
            DropIndex("dbo.Packages", new[] { "SenderID" });
            DropTable("dbo.RouteStops");
            DropTable("dbo.Users");
            DropTable("dbo.Packages");
        }
    }
}
