﻿using System.Runtime.Serialization;

namespace TrackingApplication.Services.Enums
{
    [DataContract]
    public enum LoginStatus
    {
        [EnumMember]
        RegisteredAdmin = 0,
        [EnumMember]
        RegisteredUser = 1,
        [EnumMember]
        Unregistered = 2
    }
}