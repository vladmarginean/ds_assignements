﻿using System.Collections.Generic;
using System.Linq;
using TrackingApplication.Repository.Models;
using TrackingApplication.Services.Models;

namespace TrackingApplication.Services.Helpers
{
    public static class PackageConverter
    {
        public static PackageInformation ConverFromModel(Package model)
        {
            if (model == null)
                return null;

            return new PackageInformation
            {
                ID = model.ID,
                Name = model.Name,
                Description = model.Description,
                SenderCity = model.SenderCity,
                DestinationCity = model.DestinationCity,
                Tracking = model.Tracking,
                SenderID = model.SenderID,
                ReceiverID = model.ReceiverID,
                SenderName = string.Concat(model.Sender.FirstName, " ", model.Sender.LastName),
                ReceiverName = string.Concat(model.Receiver.FirstName, " ", model.Receiver.LastName)
            };
        }

        public static List<PackageInformation> ConvertFromModel(IEnumerable<Package> models)
        {
            return models?.Select(ConverFromModel).ToList() ?? new List<PackageInformation>();
        }

        public static Package ConvertToModel(PackageInformation information)
        {
            if (information == null)
                return null;

            return new Package
            {
                //ID = information.ID,
                Name = information.Name,
                Description = information.Description,
                //SenderCity = information.SenderCity,
                //DestinationCity = information.DestinationCity,
                //Tracking = information.Tracking,
                SenderID = information.SenderID,
                ReceiverID = information.ReceiverID
            };
        }

        public static List<Package> ConvertToModel(IEnumerable<PackageInformation> informations)
        {
            return informations?.Select(ConvertToModel).ToList() ?? new List<Package>();
        }
    }
}