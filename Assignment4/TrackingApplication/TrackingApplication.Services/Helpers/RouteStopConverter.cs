﻿using System.Collections.Generic;
using System.Linq;
using TrackingApplication.Repository.Models;
using TrackingApplication.Services.Models;

namespace TrackingApplication.Services.Helpers
{
    public static class RouteStopConverter
    {
        public static RouteStopInformation ConvertFromModel(RouteStop model)
        {
            if (model == null)
                return null;

            return new RouteStopInformation
            {
                City = model.City,
                Time = model.Time,
                PackageID = model.PackageID,
                PackageName = model.Package.Name
            };
        }

        public static List<RouteStopInformation> ConvertFromModel(IEnumerable<RouteStop> models)
        {
            return models?.Select(ConvertFromModel).ToList() ?? new List<RouteStopInformation>();
        }

        public static RouteStop ConvertToModel(RouteStopInformation information)
        {
            if (information == null)
                return null;

            return new RouteStop
            {
                City = information.City,
                Time = information.Time,
                PackageID = information.PackageID
            };
        }

        public static List<RouteStop> ConvertToModel(IEnumerable<RouteStopInformation> informations)
        {
            return informations?.Select(ConvertToModel).ToList() ?? new List<RouteStop>();
        }
    }
}