﻿using System.Collections.Generic;
using System.Linq;
using TrackingApplication.Repository.Models;
using TrackingApplication.Services.Models;

namespace TrackingApplication.Services.Helpers
{
    public static class UserConverter
    {
        public static UserInformation ConvertFromModel(User model)
        {
            if (model == null)
                return null;

            return new UserInformation
            {
                ID = model.ID,
                Username = model.Username,
                FirstName = model.FirstName,
                LastName = model.LastName,
                City = model.City
            };
        }

        public static List<UserInformation> ConvertFromModel(IEnumerable<User> models)
        {
            return models?.Select(ConvertFromModel).ToList() ?? new List<UserInformation>();
        }
    }
}