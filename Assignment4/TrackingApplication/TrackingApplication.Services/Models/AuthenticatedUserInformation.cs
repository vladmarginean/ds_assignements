﻿using System.Runtime.Serialization;
using TrackingApplication.Services.Enums;

namespace TrackingApplication.Services.Models
{
    [DataContract]
    public class AuthenticatedUserInformation
    {
        [DataMember]
        public LoginStatus LoginStatus { get; set; }

        [DataMember]
        public UserInformation UserInformation { get; set; }
    }
}