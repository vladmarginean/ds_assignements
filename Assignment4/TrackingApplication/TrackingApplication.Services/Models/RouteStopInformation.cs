﻿using System;
using System.Runtime.Serialization;

namespace TrackingApplication.Services.Models
{
    [DataContract]
    public class RouteStopInformation
    {
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public int PackageID { get; set; }

        [DataMember]
        public string PackageName { get; set; }
    }
}