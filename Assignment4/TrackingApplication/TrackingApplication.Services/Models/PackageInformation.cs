﻿using System.Runtime.Serialization;

namespace TrackingApplication.Services.Models
{
    [DataContract]
    public class PackageInformation
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string SenderCity { get; set; }
        [DataMember]
        public string DestinationCity { get; set; }
        [DataMember]
        public int Tracking { get; set; }

        [DataMember]
        public int SenderID { get; set; }
        [DataMember]
        public int ReceiverID { get; set; }

        [DataMember]
        public string SenderName { get; set; }
        [DataMember]
        public string ReceiverName { get; set; }
    }
}