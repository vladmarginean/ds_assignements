﻿using System.Runtime.Serialization;

namespace TrackingApplication.Services.Models
{
    [DataContract]
    public class UserInformation
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string City { get; set; }
    }
}