﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TrackingApplication.Services.Models
{
    [DataContract]
    public class PackageStatusInformation
    {
        [DataMember]
        public PackageInformation PackageInformation { get; set; }

        [DataMember]
        public List<RouteStopInformation> RouteStopsInformation { get; set; }
    }
}