﻿using System.Runtime.Serialization;

namespace TrackingApplication.Services.Models
{
    [DataContract]
    public class LoginInformation
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}