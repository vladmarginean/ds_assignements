﻿using System.Collections.Generic;
using System.Linq;
using TrackingApplication.Repository.Models;
using TrackingApplication.Repository.Repo;
using TrackingApplication.Services.Helpers;
using TrackingApplication.Services.Models;

namespace TrackingApplication.Services.Services
{
    public class ClientOperationsService : IClientOperationsService
    {
        public List<PackageInformation> GetSentPackages(int userID, string query)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                IEnumerable<Package> packages;

                packages = unitOfWork.PackageRepository.Get(e => e.SenderID == userID);

                if (packages != null && !string.IsNullOrEmpty(query))
                {
                    packages = packages.Where(e => e.Name.Contains(query) || e.Description.Contains(query));
                }

                return PackageConverter.ConvertFromModel(packages);
            }
        }

        public List<PackageInformation> GetReceivedPackages(int userID, string query)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                IEnumerable<Package> packages;

                packages = unitOfWork.PackageRepository.Get(e => e.ReceiverID == userID);

                if (packages != null && !string.IsNullOrEmpty(query))
                {
                    packages = packages.Where(e => e.Name.Contains(query) || e.Description.Contains(query));
                }

                return PackageConverter.ConvertFromModel(packages);
            }
        }

        public PackageStatusInformation GetPackageStatus(int packageID)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                Package package;

                package = unitOfWork.PackageRepository.GetByID(packageID);

                if (package == null)
                    return null;

                IEnumerable<RouteStop> routeStops;

                routeStops = unitOfWork.RouteStopRepository.Get(e => e.PackageID == packageID);

                return new PackageStatusInformation()
                {
                    PackageInformation = PackageConverter.ConverFromModel(package),
                    RouteStopsInformation =
                        new List<RouteStopInformation>(RouteStopConverter.ConvertFromModel(routeStops))
                };
            }
        }
    }
}
