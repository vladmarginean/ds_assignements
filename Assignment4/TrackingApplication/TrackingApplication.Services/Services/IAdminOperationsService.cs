﻿using System.Collections.Generic;
using System.ServiceModel;
using TrackingApplication.Services.Models;

namespace TrackingApplication.Services.Services
{
    [ServiceContract]
    public interface IAdminOperationsService
    {
        [OperationContract]
        List<PackageInformation> GetPackages(string query);

        [OperationContract]
        void AddPackage(PackageInformation packageInformation);

        [OperationContract]
        void DeletePackage(int packageID);

        [OperationContract]
        void UpdatePackageStatus(int packageID, RouteStopInformation routeStopInformation);

        [OperationContract]
        List<UserInformation> GetUsers();
    }
}
