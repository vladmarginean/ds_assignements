﻿using System.ServiceModel;
using TrackingApplication.Services.Models;
using LoginStatus = TrackingApplication.Services.Enums.LoginStatus;

namespace TrackingApplication.Services.Services
{
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        AuthenticatedUserInformation Login(LoginInformation loginInformation);

        [OperationContract]
        bool Register(RegisterInformation registerInformation);
    }
}
