﻿using System.Collections.Generic;
using System.Linq;
using TrackingApplication.Repository.Enums;
using TrackingApplication.Repository.Models;
using TrackingApplication.Repository.Repo;
using TrackingApplication.Services.Helpers;
using TrackingApplication.Services.Models;

namespace TrackingApplication.Services.Services
{
    public class AdminOperationsService : IAdminOperationsService
    {
        public List<PackageInformation> GetPackages(string query)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                IEnumerable<Package> packages;

                packages = unitOfWork.PackageRepository.Get();

                if (packages != null && !string.IsNullOrEmpty(query))
                {
                    packages = packages.Where(e => e.Name.Contains(query) || e.Description.Contains(query));
                }

                return PackageConverter.ConvertFromModel(packages);
            }
        }

        public void AddPackage(PackageInformation packageInformation)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var package = PackageConverter.ConvertToModel(packageInformation);

                package.SenderCity = unitOfWork.UserRepository.GetByID(package.SenderID).City;
                package.DestinationCity = unitOfWork.UserRepository.GetByID(package.ReceiverID).City;
                package.Tracking = (int) PackageTracking.Untracked;

                unitOfWork.PackageRepository.Insert(package);
            }
        }

        public void DeletePackage(int packageID)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var routeStops = unitOfWork.RouteStopRepository.Get(e => e.PackageID == packageID);

                foreach (var routeStop in routeStops)
                {
                    unitOfWork.RouteStopRepository.Delete(routeStop);
                }

                unitOfWork.PackageRepository.Delete(packageID);
            }
        }

        public void UpdatePackageStatus(int packageID, RouteStopInformation routeStopInformation)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                Package package;

                package = unitOfWork.PackageRepository.GetByID(packageID);

                if (package == null || routeStopInformation == null)
                    return;

                var routeStop = RouteStopConverter.ConvertToModel(routeStopInformation);

                if (package.Tracking == (int) PackageTracking.Untracked)
                {
                    package.Tracking = (int) PackageTracking.Tracked;
                    unitOfWork.PackageRepository.Update(package);
                }

                unitOfWork.RouteStopRepository.Insert(routeStop);
            }
        }

        public List<UserInformation> GetUsers()
        {
            using (var unitOfWork = new UnitOfWork())
            {
                IEnumerable<User> users;

                users = unitOfWork.UserRepository.Get(e => e.Role != (int) UserRole.Admin);

                return UserConverter.ConvertFromModel(users);
            }
        }
    }
}
