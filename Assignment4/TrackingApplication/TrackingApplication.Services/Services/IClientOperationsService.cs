﻿using System.Collections.Generic;
using System.ServiceModel;
using TrackingApplication.Services.Models;

namespace TrackingApplication.Services.Services
{
    [ServiceContract]
    public interface IClientOperationsService
    {
        [OperationContract]
        List<PackageInformation> GetSentPackages(int userID, string query);

        [OperationContract]
        List<PackageInformation> GetReceivedPackages(int userID, string query);

        [OperationContract]
        PackageStatusInformation GetPackageStatus(int packageID);
    }
}
