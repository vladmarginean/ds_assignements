﻿using System.Linq;
using TrackingApplication.Repository.Enums;
using TrackingApplication.Repository.Models;
using TrackingApplication.Repository.Repo;
using TrackingApplication.Services.Helpers;
using TrackingApplication.Services.Models;
using LoginStatus = TrackingApplication.Services.Enums.LoginStatus;

namespace TrackingApplication.Services.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        public AuthenticatedUserInformation Login(LoginInformation loginInformation)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                User user;

                user = unitOfWork.UserRepository
                    .Get(e => e.Username == loginInformation.Username && e.Password == loginInformation.Password)
                    .FirstOrDefault();

                if (user == null)
                    return new AuthenticatedUserInformation {LoginStatus = LoginStatus.Unregistered};

                var loginStatus = user.Role == (int) UserRole.Admin
                    ? LoginStatus.RegisteredAdmin
                    : LoginStatus.RegisteredUser;

                return new AuthenticatedUserInformation
                {
                    LoginStatus = loginStatus,
                    UserInformation = UserConverter.ConvertFromModel(user)
                };
            }
        }

        public bool Register(RegisterInformation registerInformation)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var username = registerInformation.Username;
                var password = registerInformation.Password;
                var city = registerInformation.City;

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(city))
                    return false;

                User user;

                user = unitOfWork.UserRepository
                    .Get(e => e.Username == username)
                    .FirstOrDefault();

                if (user != null)
                    return false;

                user = new User
                {
                    Username = username,
                    Password = password,
                    FirstName = registerInformation.FirstName,
                    LastName = registerInformation.LastName,
                    Address = registerInformation.Address,
                    City = city,
                    Role = (int) UserRole.User
                };

                var insertStatus = unitOfWork.UserRepository.Insert(user);

                return insertStatus;
            }
        }
    }
}
