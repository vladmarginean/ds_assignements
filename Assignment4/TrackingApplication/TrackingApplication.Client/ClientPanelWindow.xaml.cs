﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using TrackingApplication.Client.AuthenticationServiceReference;
using TrackingApplication.Client.ClientOperationsServiceReference;

namespace TrackingApplication.Client
{
    /// <summary>
    /// Interaction logic for ClientPanelWindow.xaml
    /// </summary>
    public partial class ClientPanelWindow : Window
    {
        private readonly UserInformation _userInformation;
        private IClientOperationsService _clientOperationsService = new ClientOperationsServiceClient();
        private IEnumerable<PackageInformation> _sentPackages;
        private IEnumerable<PackageInformation> _filteredSentPackages;
        private IEnumerable<PackageInformation> _receivedPackages;
        private IEnumerable<PackageInformation> _filteredReceivedPackages;

        private ClientPanelWindow()
        {
            InitializeComponent();
        }

        public ClientPanelWindow(UserInformation userInformation) : this()
        {
            _userInformation = userInformation;
        }

        private void ClientPanelWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            _sentPackages = _clientOperationsService.GetSentPackages(_userInformation.ID, null);
            SentPackagesDataGrid.DataContext = _sentPackages;

            _receivedPackages = _clientOperationsService.GetReceivedPackages(_userInformation.ID, null);
            ReceivedPackagesDataGrid.DataContext = _receivedPackages;

            NameTextBox.DataContext = _userInformation.FirstName;
        }

        private void LogoutButton_OnClick(object sender, RoutedEventArgs e)
        {
            var loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }

        private void SentPackagesButton_OnClick(object sender, RoutedEventArgs e)
        {
            SentPackagesTextBox.Text = "";

            _sentPackages = _clientOperationsService.GetSentPackages(_userInformation.ID, null);
            SentPackagesDataGrid.DataContext = _sentPackages;
        }

        private void ReceivedPackagesButton_OnClick(object sender, RoutedEventArgs e)
        {
            ReceivedPackagesTextBox.Text = "";

            _receivedPackages = _clientOperationsService.GetReceivedPackages(_userInformation.ID, null);
            ReceivedPackagesDataGrid.DataContext = _receivedPackages;
        }

        private void SentPackagesTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(SentPackagesTextBox.Text))
            {
                _filteredSentPackages = _sentPackages;
            }
            else
            {
                _filteredSentPackages = _sentPackages
                    .Where(
                        t =>
                            t.Name.Contains(SentPackagesTextBox.Text, StringComparison.OrdinalIgnoreCase) ||
                            t.Description.Contains(SentPackagesTextBox.Text, StringComparison.OrdinalIgnoreCase));
            }

            SentPackagesDataGrid.DataContext = _filteredSentPackages;
        }

        private void ReceivedPackagesTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(ReceivedPackagesTextBox.Text))
            {
                _filteredReceivedPackages = _receivedPackages;
            }
            else
            {
                _filteredReceivedPackages = _receivedPackages
                    .Where(
                        t =>
                            t.Name.Contains(ReceivedPackagesTextBox.Text, StringComparison.OrdinalIgnoreCase) ||
                            t.Description.Contains(ReceivedPackagesTextBox.Text, StringComparison.OrdinalIgnoreCase));
            }

            ReceivedPackagesDataGrid.DataContext = _filteredReceivedPackages;
        }

        private void SentPackagesStatusButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedPackage = SentPackagesDataGrid.SelectedItem as PackageInformation;
            
            ShowPackageStatus(selectedPackage);
        }

        private void ReceivedPackagesStatusButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedPackage = ReceivedPackagesDataGrid.SelectedItem as PackageInformation;

            ShowPackageStatus(selectedPackage);
        }

        private void ShowPackageStatus(PackageInformation selectedPackage)
        {
            if (selectedPackage == null)
            {
                MessageBox.Show("You must select a package first!");
                return;
            }

            var package = _clientOperationsService.GetPackageStatus(selectedPackage.ID);

            MessageBox.Show(StatusMessageHelper.ConstrutStatusMessage(package), "Package status");
        }
    }

    public static class StringExtensions
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}
