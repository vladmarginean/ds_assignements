﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using TrackingApplication.Client.AuthenticationServiceReference;

namespace TrackingApplication.Client
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private IAuthenticationService _authenticationService = new AuthenticationServiceClient();

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_OnClick(object sender, RoutedEventArgs e)
        {
            var username = UsernameTextBox.Text;
            var password = Marshal.PtrToStringUni(Marshal.SecureStringToGlobalAllocUnicode(PasswordTextBox.SecurePassword));

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Username and password should not be empty!");
                return;
            }

            var loginInformation = new LoginInformation
            {
                Username = username,
                Password = password
            };

            var authenticatedUserInformation = _authenticationService.Login(loginInformation);

            switch (authenticatedUserInformation.LoginStatus)
            {
                case LoginStatus.RegisteredAdmin:
                    var adminPanelWindow = new AdminPanelWindow(authenticatedUserInformation.UserInformation);
                    adminPanelWindow.Show();
                    Close();
                    break;
                case LoginStatus.RegisteredUser:
                    var clientPanelWindow = new ClientPanelWindow(authenticatedUserInformation.UserInformation);
                    clientPanelWindow.Show();
                    Close();
                    break;
                case LoginStatus.Unregistered:
                    MessageBox.Show("No account with this credentials. Register!");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void RegisterButton_OnClick(object sender, RoutedEventArgs e)
        {
            var registerWindow = new RegisterWindow();
            registerWindow.Show();
            Close();
        }
    }
}
