﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TrackingApplication.Client.ClientOperationsServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PackageInformation", Namespace="http://schemas.datacontract.org/2004/07/TrackingApplication.Services.Models")]
    [System.SerializableAttribute()]
    public partial class PackageInformation : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DestinationCityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ReceiverIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ReceiverNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SenderCityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int SenderIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SenderNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int TrackingField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description {
            get {
                return this.DescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true)) {
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DestinationCity {
            get {
                return this.DestinationCityField;
            }
            set {
                if ((object.ReferenceEquals(this.DestinationCityField, value) != true)) {
                    this.DestinationCityField = value;
                    this.RaisePropertyChanged("DestinationCity");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReceiverID {
            get {
                return this.ReceiverIDField;
            }
            set {
                if ((this.ReceiverIDField.Equals(value) != true)) {
                    this.ReceiverIDField = value;
                    this.RaisePropertyChanged("ReceiverID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ReceiverName {
            get {
                return this.ReceiverNameField;
            }
            set {
                if ((object.ReferenceEquals(this.ReceiverNameField, value) != true)) {
                    this.ReceiverNameField = value;
                    this.RaisePropertyChanged("ReceiverName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SenderCity {
            get {
                return this.SenderCityField;
            }
            set {
                if ((object.ReferenceEquals(this.SenderCityField, value) != true)) {
                    this.SenderCityField = value;
                    this.RaisePropertyChanged("SenderCity");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SenderID {
            get {
                return this.SenderIDField;
            }
            set {
                if ((this.SenderIDField.Equals(value) != true)) {
                    this.SenderIDField = value;
                    this.RaisePropertyChanged("SenderID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SenderName {
            get {
                return this.SenderNameField;
            }
            set {
                if ((object.ReferenceEquals(this.SenderNameField, value) != true)) {
                    this.SenderNameField = value;
                    this.RaisePropertyChanged("SenderName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Tracking {
            get {
                return this.TrackingField;
            }
            set {
                if ((this.TrackingField.Equals(value) != true)) {
                    this.TrackingField = value;
                    this.RaisePropertyChanged("Tracking");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PackageStatusInformation", Namespace="http://schemas.datacontract.org/2004/07/TrackingApplication.Services.Models")]
    [System.SerializableAttribute()]
    public partial class PackageStatusInformation : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation PackageInformationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private TrackingApplication.Client.ClientOperationsServiceReference.RouteStopInformation[] RouteStopsInformationField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation PackageInformation {
            get {
                return this.PackageInformationField;
            }
            set {
                if ((object.ReferenceEquals(this.PackageInformationField, value) != true)) {
                    this.PackageInformationField = value;
                    this.RaisePropertyChanged("PackageInformation");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public TrackingApplication.Client.ClientOperationsServiceReference.RouteStopInformation[] RouteStopsInformation {
            get {
                return this.RouteStopsInformationField;
            }
            set {
                if ((object.ReferenceEquals(this.RouteStopsInformationField, value) != true)) {
                    this.RouteStopsInformationField = value;
                    this.RaisePropertyChanged("RouteStopsInformation");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RouteStopInformation", Namespace="http://schemas.datacontract.org/2004/07/TrackingApplication.Services.Models")]
    [System.SerializableAttribute()]
    public partial class RouteStopInformation : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int PackageIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PackageNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime TimeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string City {
            get {
                return this.CityField;
            }
            set {
                if ((object.ReferenceEquals(this.CityField, value) != true)) {
                    this.CityField = value;
                    this.RaisePropertyChanged("City");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int PackageID {
            get {
                return this.PackageIDField;
            }
            set {
                if ((this.PackageIDField.Equals(value) != true)) {
                    this.PackageIDField = value;
                    this.RaisePropertyChanged("PackageID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PackageName {
            get {
                return this.PackageNameField;
            }
            set {
                if ((object.ReferenceEquals(this.PackageNameField, value) != true)) {
                    this.PackageNameField = value;
                    this.RaisePropertyChanged("PackageName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime Time {
            get {
                return this.TimeField;
            }
            set {
                if ((this.TimeField.Equals(value) != true)) {
                    this.TimeField = value;
                    this.RaisePropertyChanged("Time");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ClientOperationsServiceReference.IClientOperationsService")]
    public interface IClientOperationsService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientOperationsService/GetSentPackages", ReplyAction="http://tempuri.org/IClientOperationsService/GetSentPackagesResponse")]
        TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[] GetSentPackages(int userID, string query);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientOperationsService/GetSentPackages", ReplyAction="http://tempuri.org/IClientOperationsService/GetSentPackagesResponse")]
        System.Threading.Tasks.Task<TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[]> GetSentPackagesAsync(int userID, string query);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientOperationsService/GetReceivedPackages", ReplyAction="http://tempuri.org/IClientOperationsService/GetReceivedPackagesResponse")]
        TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[] GetReceivedPackages(int userID, string query);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientOperationsService/GetReceivedPackages", ReplyAction="http://tempuri.org/IClientOperationsService/GetReceivedPackagesResponse")]
        System.Threading.Tasks.Task<TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[]> GetReceivedPackagesAsync(int userID, string query);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientOperationsService/GetPackageStatus", ReplyAction="http://tempuri.org/IClientOperationsService/GetPackageStatusResponse")]
        TrackingApplication.Client.ClientOperationsServiceReference.PackageStatusInformation GetPackageStatus(int packageID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientOperationsService/GetPackageStatus", ReplyAction="http://tempuri.org/IClientOperationsService/GetPackageStatusResponse")]
        System.Threading.Tasks.Task<TrackingApplication.Client.ClientOperationsServiceReference.PackageStatusInformation> GetPackageStatusAsync(int packageID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IClientOperationsServiceChannel : TrackingApplication.Client.ClientOperationsServiceReference.IClientOperationsService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ClientOperationsServiceClient : System.ServiceModel.ClientBase<TrackingApplication.Client.ClientOperationsServiceReference.IClientOperationsService>, TrackingApplication.Client.ClientOperationsServiceReference.IClientOperationsService {
        
        public ClientOperationsServiceClient() {
        }
        
        public ClientOperationsServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ClientOperationsServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ClientOperationsServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ClientOperationsServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[] GetSentPackages(int userID, string query) {
            return base.Channel.GetSentPackages(userID, query);
        }
        
        public System.Threading.Tasks.Task<TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[]> GetSentPackagesAsync(int userID, string query) {
            return base.Channel.GetSentPackagesAsync(userID, query);
        }
        
        public TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[] GetReceivedPackages(int userID, string query) {
            return base.Channel.GetReceivedPackages(userID, query);
        }
        
        public System.Threading.Tasks.Task<TrackingApplication.Client.ClientOperationsServiceReference.PackageInformation[]> GetReceivedPackagesAsync(int userID, string query) {
            return base.Channel.GetReceivedPackagesAsync(userID, query);
        }
        
        public TrackingApplication.Client.ClientOperationsServiceReference.PackageStatusInformation GetPackageStatus(int packageID) {
            return base.Channel.GetPackageStatus(packageID);
        }
        
        public System.Threading.Tasks.Task<TrackingApplication.Client.ClientOperationsServiceReference.PackageStatusInformation> GetPackageStatusAsync(int packageID) {
            return base.Channel.GetPackageStatusAsync(packageID);
        }
    }
}
