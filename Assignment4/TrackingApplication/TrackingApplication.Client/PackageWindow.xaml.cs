﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackingApplication.Client.AdminOperationsServiceReference;

namespace TrackingApplication.Client
{
    /// <summary>
    /// Interaction logic for PackageWindow.xaml
    /// </summary>
    public partial class PackageWindow : Window
    {
        private IAdminOperationsService _adminOperationsService = new AdminOperationsServiceClient();
        private IEnumerable<UserInformation> _users;

        public PackageWindow()
        {
            InitializeComponent();
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var name = NameTextBox.Text;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Must specify name!");
                return;
            }

            var description = DescriptionTextBox.Text;

            var senderID = (int?) SenderComboBox.SelectedValue;

            if (senderID == null)
            {
                MessageBox.Show("Must specify sender!");
                return;
            }

            var receiverID = (int?) ReceiverComboBox.SelectedValue;

            if (receiverID == null)
            {
                MessageBox.Show("Must specify receiver!");
                return;
            }

            if (senderID.Value == receiverID.Value)
            {
                MessageBox.Show("Error! Can't send stuff to you!");
                return;
            }

            var packageInformation = new PackageInformation
            {
                Name = name,
                Description = description,
                SenderID = senderID.Value,
                ReceiverID = receiverID.Value
            };

            _adminOperationsService.AddPackage(packageInformation);

            MessageBox.Show("Success! This window will now close!");
            Close();
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void PackageWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            _users = _adminOperationsService.GetUsers();

            SenderComboBox.DataContext = _users;
            SenderComboBox.SelectedValuePath = "ID";

            ReceiverComboBox.DataContext = _users;
            ReceiverComboBox.SelectedValuePath = "ID";
        }
    }
}
