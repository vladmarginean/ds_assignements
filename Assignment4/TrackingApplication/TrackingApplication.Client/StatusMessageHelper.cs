﻿using System.Text;
using TrackingApplication.Client.ClientOperationsServiceReference;

namespace TrackingApplication.Client
{
    public static class StatusMessageHelper
    {
        public static string ConstrutStatusMessage(PackageStatusInformation packageStatus)
        {
            var package = packageStatus.PackageInformation;

            var messageBuilder = new StringBuilder();
            messageBuilder.Append("Name: ");
            messageBuilder.Append(package.Name);
            messageBuilder.Append("\n");
            messageBuilder.Append("Description: ");
            messageBuilder.Append(package.Description);
            messageBuilder.Append("\n");
            messageBuilder.Append("From: ");
            messageBuilder.Append(package.SenderName);
            messageBuilder.Append(" (" + package.SenderCity + ")");
            messageBuilder.Append("\n");
            messageBuilder.Append("To: ");
            messageBuilder.Append(package.ReceiverName);
            messageBuilder.Append(" (" + package.DestinationCity + ")");
            messageBuilder.Append("\n");
            messageBuilder.Append("Tracking: ");
            messageBuilder.Append(package.Tracking == 0 ? "Untracked" : "Tracked");
            messageBuilder.Append("\n\n");
            messageBuilder.Append("Route:");
            messageBuilder.Append("\n");

            foreach (var routeStop in packageStatus.RouteStopsInformation)
            {
                messageBuilder.Append(routeStop.City);
                messageBuilder.Append(" (" + routeStop.Time + ")");
                messageBuilder.Append("\n");
            }

            if (packageStatus.RouteStopsInformation.Length == 0)
                messageBuilder.Append("N/A");

            return messageBuilder.ToString();
        }
    }
}
