﻿using System.Runtime.InteropServices;
using System.Windows;
using TrackingApplication.Client.AuthenticationServiceReference;

namespace TrackingApplication.Client
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        private IAuthenticationService _authenticationService = new AuthenticationServiceClient();

        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void RegisterButton_OnClick(object sender, RoutedEventArgs e)
        {
            var username = UsernameTextBox.Text;

            if (string.IsNullOrEmpty(username))
            {
                MessageBox.Show("Must specify username!");
                return;
            }

            var password = Marshal.PtrToStringUni(Marshal.SecureStringToGlobalAllocUnicode(PasswordTextBox.SecurePassword));

            if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Must specify password!");
                return;
            }

            var firstName = FirstNameTextBox.Text;

            if (string.IsNullOrEmpty(firstName))
            {
                MessageBox.Show("Must specify first name!");
                return;
            }

            var lastName = LastNameTextBox.Text;

            if (string.IsNullOrEmpty(lastName))
            {
                MessageBox.Show("Must specify last name!");
                return;
            }

            var address = AddressTextBox.Text;

            var city = CityTextBox.Text;

            if (string.IsNullOrEmpty(city))
            {
                MessageBox.Show("Must specify city!");
                return;
            }

            var registerInformation = new RegisterInformation
            {
                Username = username,
                Password = password,
                FirstName = firstName,
                LastName = lastName,
                Address = address,
                City = city
            };

            var registerStatus = _authenticationService.Register(registerInformation);

            if (registerStatus)
            {
                MessageBox.Show("Success! You will be redirected to log in!");

                var loginWindow = new LoginWindow();
                loginWindow.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Error! Please retry!");
            }
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            var loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }
    }
}
