﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TrackingApplication.Client.AdminOperationsServiceReference;
using TrackingApplication.Client.ClientOperationsServiceReference;
using PackageInformation = TrackingApplication.Client.AdminOperationsServiceReference.PackageInformation;
using UserInformation = TrackingApplication.Client.AuthenticationServiceReference.UserInformation;

namespace TrackingApplication.Client
{
    /// <summary>
    /// Interaction logic for AdminPanelWindow.xaml
    /// </summary>
    public partial class AdminPanelWindow : Window
    {
        private readonly UserInformation _userInformation;
        private IAdminOperationsService _adminOperationsService = new AdminOperationsServiceClient();
        private IClientOperationsService _clientOperationsService = new ClientOperationsServiceClient();
        private IEnumerable<PackageInformation> _packages;
        private IEnumerable<PackageInformation> _filteredPackages;

        public AdminPanelWindow()
        {
            InitializeComponent();
        }

        public AdminPanelWindow(UserInformation userInformation) : this()
        {
            _packages = _adminOperationsService.GetPackages(null);
            PackagesDataGrid.DataContext = _packages;

            _userInformation = userInformation;
        }

        private void LogoutButton_OnClick(object sender, RoutedEventArgs e)
        {
            var loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }

        private void AdminPanelWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            NameTextBox.DataContext = _userInformation.Username;
        }

        private void PackagesButton_OnClick(object sender, RoutedEventArgs e)
        {
            PackagesTextBox.Text = "";

            _packages = _adminOperationsService.GetPackages(null);
            PackagesDataGrid.DataContext = _packages;
        }

        private void PackagesDeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedPackage = PackagesDataGrid.SelectedItem as PackageInformation;

            if (selectedPackage == null)
            {
                MessageBox.Show("You must select a package first!");
                return;
            }

            _adminOperationsService.DeletePackage(selectedPackage.ID);

            MessageBox.Show("Success! The package was deleted!");

            PackagesTextBox.Text = "";

            _packages = _adminOperationsService.GetPackages(null);
            PackagesDataGrid.DataContext = _packages;
        }

        private void PackagesStatusButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedPackage = PackagesDataGrid.SelectedItem as PackageInformation;

            ShowPackageStatus(selectedPackage);
        }

        private void ShowPackageStatus(PackageInformation selectedPackage)
        {
            if (selectedPackage == null)
            {
                MessageBox.Show("You must select a package first!");
                return;
            }

            var package = _clientOperationsService.GetPackageStatus(selectedPackage.ID);

            MessageBox.Show(StatusMessageHelper.ConstrutStatusMessage(package), "Package status");
        }

        private void PackagesStatusUpdateButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedPackage = PackagesDataGrid.SelectedItem as PackageInformation;

            if (selectedPackage == null)
            {
                MessageBox.Show("You must select a package first!");
                return;
            }

            if (selectedPackage.Tracking == 0)
            {
                MessageBox.Show("Package is not tracked! Update tracking via the Tracking button!");
                return;
            }

            var routeStopWindow = new RouteStopWindow(selectedPackage.ID);
            routeStopWindow.ShowDialog();
        }

        private void PackagesTrackingUpdateButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedPackage = PackagesDataGrid.SelectedItem as PackageInformation;

            if (selectedPackage == null)
            {
                MessageBox.Show("You must select a package first!");
                return;
            }

            if (selectedPackage.Tracking == 1)
            {
                MessageBox.Show("Package is already tracked! Update route via the Status button!");
                return;
            }

            var routeStopWindow = new RouteStopWindow(selectedPackage.ID);
            routeStopWindow.ShowDialog();

            PackagesTextBox.Text = "";

            _packages = _adminOperationsService.GetPackages(null);
            PackagesDataGrid.DataContext = _packages;
        }

        private void PackagesAddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var packageWindow = new PackageWindow();
            packageWindow.ShowDialog();

            PackagesTextBox.Text = "";

            _packages = _adminOperationsService.GetPackages(null);
            PackagesDataGrid.DataContext = _packages;
        }

        private void PackagesTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(PackagesTextBox.Text))
            {
                _filteredPackages = _packages;
            }
            else
            {
                _filteredPackages = _packages
                    .Where(
                        t =>
                            t.Name.Contains(PackagesTextBox.Text, StringComparison.OrdinalIgnoreCase) ||
                            t.Description.Contains(PackagesTextBox.Text, StringComparison.OrdinalIgnoreCase));
            }

            PackagesDataGrid.DataContext = _filteredPackages;
        }
    }
}
