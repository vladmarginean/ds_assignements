﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackingApplication.Client.AdminOperationsServiceReference;

namespace TrackingApplication.Client
{
    /// <summary>
    /// Interaction logic for RouteStopWindow.xaml
    /// </summary>
    public partial class RouteStopWindow : Window
    {
        private readonly int _packageID;
        private IAdminOperationsService _adminOperationsService = new AdminOperationsServiceClient();

        public RouteStopWindow()
        {
            InitializeComponent();
        }

        public RouteStopWindow(int packageID) : this()
        {
            _packageID = packageID;
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var city = CityTextBox.Text;

            if (string.IsNullOrEmpty(city))
            {
                MessageBox.Show("Must specify city!");
                return;
            }

            var inputDate = DateDatePicker.SelectedDate;

            if (inputDate == null)
            {
                MessageBox.Show("Must specify date!");
                return;
            }

            var selectedDate = inputDate.Value;

            var inputTime = TimeTextBox.Text;

            if (string.IsNullOrEmpty(inputTime))
            {
                MessageBox.Show("Must specify time!");
                return;
            }

            DateTime selectedTime;

            var success = DateTime.TryParseExact(inputTime, "HH:mm", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out selectedTime);
            if (!success)
            {
                success = DateTime.TryParseExact(inputTime, "HH:mm:ss", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out selectedTime);
                if (!success)
                {
                    MessageBox.Show("Invalid time format! Should be HH:mm/HH:mm:ss");
                    return;
                }
            }

            var time = new DateTime(selectedDate.Year, selectedDate.Month, selectedDate.Day,
                selectedTime.Hour, selectedTime.Minute, selectedTime.Second);

            var routeStopInformation = new RouteStopInformation
            {
                City = city,
                Time = time,
                PackageID = _packageID
            };

            _adminOperationsService.UpdatePackageStatus(_packageID, routeStopInformation);

            MessageBox.Show("Success! This window will now close!");
            Close();
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
