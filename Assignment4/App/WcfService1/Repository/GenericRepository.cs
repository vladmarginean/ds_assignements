﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Repository
{
    public class GenericRepository : IGenericRepository
    {
        private PackageTrackingContext Context;

        public GenericRepository(PackageTrackingContext context)
        {
            Context = context;
        }

        public IQueryable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            return Context.Set<TEntity>().AsQueryable();
        }

        public IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return Context.Set<TEntity>().Where(predicate);
        }

        public TEntity GetById<TEntity>(int id) where TEntity : class
        {
            return Context.Set<TEntity>().Find(id);
        }

        public bool Insert<TEntity>(TEntity entity) where TEntity : class
        {
            try
            {
                Context.Set<TEntity>().Add(entity);
                int rows = Context.SaveChanges();
                if (rows == 0)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public void Update<TEntity>(TEntity entityToUpdate) where TEntity : class
        {
            try
            {
                Context.Set<TEntity>().Attach(entityToUpdate);
                Context.Entry(entityToUpdate).State = EntityState.Modified;
                Context.SaveChanges();
            }
            catch (Exception e)
            {
            }
        }

        public void Delete<TEntity>(int id) where TEntity : class
        {
            try
            {
                TEntity entityToDelete = Context.Set<TEntity>().Find(id);
                if (Context.Entry(entityToDelete).State == EntityState.Detached)
                {
                    Context.Set<TEntity>().Attach(entityToDelete);
                }
                Context.Set<TEntity>().Remove(entityToDelete);
                Context.SaveChanges();
            }
            catch (Exception e)
            {
            }
        }
    }
}
