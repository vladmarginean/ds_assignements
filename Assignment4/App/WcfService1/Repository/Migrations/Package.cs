﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Models
{
    public class Package
    {
        public int PackageID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Tracking { get; set; }
        public string SenderCity { get; set; }
        public string DestinationCity { get; set; }
        public int SenderID { get; set; }
        public int ReceiverID { get; set; }
        [ForeignKey("SenderID")]
        public virtual User Sender { get; set; }
        [ForeignKey("ReceiverID")]
        public virtual User Receiver { get; set; }
        public List<RoutePoint> TrackingRoute { get; set; }
    }
}
