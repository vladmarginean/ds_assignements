namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Packages",
                c => new
                    {
                        PackageID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Tracking = c.Boolean(nullable: false),
                        SenderCity = c.String(),
                        DestinationCity = c.String(),
                        SenderID = c.Int(nullable: false),
                        ReceiverID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PackageID)
                .ForeignKey("dbo.Users", t => t.ReceiverID)
                .ForeignKey("dbo.Users", t => t.SenderID)
                .Index(t => t.SenderID)
                .Index(t => t.ReceiverID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.RoutePoints",
                c => new
                    {
                        RoutePointID = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        City = c.String(),
                        PackageID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoutePointID)
                .ForeignKey("dbo.Packages", t => t.PackageID)
                .Index(t => t.PackageID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoutePoints", "PackageID", "dbo.Packages");
            DropForeignKey("dbo.Packages", "SenderID", "dbo.Users");
            DropForeignKey("dbo.Packages", "ReceiverID", "dbo.Users");
            DropIndex("dbo.RoutePoints", new[] { "PackageID" });
            DropIndex("dbo.Packages", new[] { "ReceiverID" });
            DropIndex("dbo.Packages", new[] { "SenderID" });
            DropTable("dbo.RoutePoints");
            DropTable("dbo.Users");
            DropTable("dbo.Packages");
        }
    }
}
