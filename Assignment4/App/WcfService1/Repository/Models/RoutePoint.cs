﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Models
{
    public class RoutePoint
    {
        public int RoutePointID { get; set; }
        public DateTime Time { get; set; }
        public string City { get; set; }
        public int PackageID { get; set; }
        [ForeignKey("PackageID")]
        public Package Package { get; set; }
    }
}
