﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Repository
{
    public interface IGenericRepository
    {
        IQueryable<TEntity> GetAll<TEntity>() where TEntity : class;
        IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;
        TEntity GetById<TEntity>(int id) where TEntity : class;
        bool Insert<TEntity>(TEntity entity) where TEntity : class;
        void Update<TEntity>(TEntity entityToUpdate) where TEntity : class;
        void Delete<TEntity>(int id) where TEntity : class;
    }
}
