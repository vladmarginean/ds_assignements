﻿using Repository.Models;
using System.Data.Entity;

namespace Repository
{
    public class PackageTrackingContext : DbContext
    {
        public PackageTrackingContext()
            : base("PackageTracking")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<RoutePoint> RoutePoints { get; set; }
    }
}
