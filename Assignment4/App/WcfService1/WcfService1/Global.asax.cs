﻿using System;
using Ninject;
using Ninject.Web.Common;
using Services;

namespace Services
{
    public class Global : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            return new StandardKernel(new WcfNinjectModule());
        }
    }
}