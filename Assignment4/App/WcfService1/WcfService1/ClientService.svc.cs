﻿using Repository;
using Repository.Models;
using Services.Helpers;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ClientService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ClientService.svc or ClientService.svc.cs at the Solution Explorer and start debugging.
    public class ClientService : IClientService
    {
        private IGenericRepository repository;
        public ClientService(IGenericRepository repo)
        {
            repository = repo;
        }

        public AuthenticationStatus Login(LoginInfo user)
        {
            if (user != null)
            {
                var dbUser = repository.Query<User>(u => u.Username == user.Username && u.Password == user.Password).FirstOrDefault();
                if (dbUser != null)
                {
                    var status = new AuthenticationStatus();
                    status.UserID = dbUser.UserID;
                    status.Username = dbUser.Username;
                    status.Role = (AuthenticationRoles)dbUser.Role;
                    return status;
                }                
            }
            var response = new AuthenticationStatus() { Role = AuthenticationRoles.NotAuthenticated };
            return response;
        }

        public bool Register(RegisterInformation registerInfo)
        {
            var checkUser = repository.Query<User>(u => u.Email == registerInfo.Email || u.Username == registerInfo.Username).FirstOrDefault();
            if (checkUser != null)
            {
                return false;
            }
            var user = MappingService.MapToUser(registerInfo);
            if (user != null)
            {
                user.Role = (int)AuthenticationRoles.Client;
                var status = repository.Insert(user);
                if (!status)
                {
                    return false;
                }
            }
            return true;
        }

        public List<PackageInformation> GetSentPackages(int clientId, string searchString)
        {
            var packages = repository.Query<Package>(p => p.SenderID == clientId
                                                          && (searchString == null 
                                                              || p.Name.Contains(searchString) 
                                                              || p.Description.Contains(searchString)));
            return MappingService.MapToPackageInformationList(packages);
        }

        public List<PackageInformation> GetReceivedPackages(int clientId, string searchString)
        {
            var packages = repository.Query<Package>(p => p.ReceiverID == clientId
                                                          && (searchString == null
                                                              || p.Name.Contains(searchString)
                                                              || p.Description.Contains(searchString)));
            return MappingService.MapToPackageInformationList(packages);
        }

        public PackageStatus GetPackageStatus(int packageId)
        {
            var packageStatus = new PackageStatus();
            var dbPackage = repository.GetById<Package>(packageId);
            packageStatus.PackageInfo = MappingService.MapToPackageInformation(dbPackage);
            var routePoints = repository.Query<RoutePoint>(rp => rp.PackageID == packageId).OrderByDescending(rp => rp.Time);
            packageStatus.RoutePoints = MappingService.MapToRoutePointInfoList(routePoints);
            return packageStatus;
        }
    }
}
