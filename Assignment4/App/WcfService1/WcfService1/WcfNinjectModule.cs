﻿using Ninject.Modules;
using Repository;

namespace Services
{
    public class WcfNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGenericRepository>().To<GenericRepository>();
        }        
    }
}