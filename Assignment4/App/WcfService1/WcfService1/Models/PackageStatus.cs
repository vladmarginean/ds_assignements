﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Services.Models
{
    [DataContract]
    public class PackageStatus
    {
        [DataMember]
        public PackageInformation PackageInfo { get; set; }
        [DataMember]
        public List<RoutePointInfo> RoutePoints { get; set; }
    }
}