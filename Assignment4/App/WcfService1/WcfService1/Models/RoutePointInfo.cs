﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Services.Models
{
    [DataContract]
    public class RoutePointInfo
    {
        [DataMember]
        public int RoutePointID { get; set; }
        [DataMember]
        public DateTime Time { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public int PackageID { get; set; }
    }
}