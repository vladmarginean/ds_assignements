﻿using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Services.Models
{
    [DataContract]
    public class AuthenticationStatus
    {
        [DataMember]
        public int UserID { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public AuthenticationRoles Role { get; set; }
    }
}