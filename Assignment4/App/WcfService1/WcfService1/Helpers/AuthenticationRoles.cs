﻿using System;
using System.Runtime.Serialization;

namespace Services.Helpers
{
    [DataContract]
    public enum AuthenticationRoles
    {
        [EnumMember]
        Admin = 1,
        [EnumMember]
        Client = 2,
        [EnumMember]
        NotAuthenticated = 3
    }
}