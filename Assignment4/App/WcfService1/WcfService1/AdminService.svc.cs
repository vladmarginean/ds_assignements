﻿using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Repository.Models;
using Services.Models;
using Services.Helpers;

namespace Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AdminService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AdminService.svc or AdminService.svc.cs at the Solution Explorer and start debugging.
    public class AdminService : IAdminService
    {
        private IGenericRepository repository;
        public AdminService(IGenericRepository repo)
        {
            repository = repo;
        }

        public List<PackageInformation> GetAllPackages(int userId)
        {
            var user = repository.GetById<User>(userId);
            if (user != null)
            {
                var packages = repository.Query<Package>(p => user.Role == (int)AuthenticationRoles.Admin
                                                           || p.SenderID == userId
                                                           || p.ReceiverID == userId);
                return MappingService.MapToPackageInformationList(packages);
            }
            return null;
        }

        public List<ClientInformation> GetClients()
        {
            var clients = repository.Query<User>(u => u.Role == (int)AuthenticationRoles.Client).ToList();
            return MappingService.MapToUserInformationList(clients);
        }

        public bool AddPackage(PackageInformation packageInfo)
        {
            var package = MappingService.MapToPackage(packageInfo);
            package.SenderCity = repository.Query<User>(u => u.UserID == package.SenderID)
                                           .Select(u => u.City)
                                           .FirstOrDefault();
            package.DestinationCity = repository.Query<User>(u => u.UserID == package.ReceiverID)
                                                .Select(u => u.City)
                                                .FirstOrDefault();
            if (package == null)
            {
                return false;
            }
            var status = repository.Insert(package);
            return status;
        }

        public bool AddPackageRoutePoint(RoutePointInfo routePointInfo)
        {
            if (routePointInfo == null)
            {
                return false;
            }
            var rp = MappingService.MapToRoutePont(routePointInfo);
            var status = repository.Insert(rp);
            return status;
        }

        public bool DeletePackage(int packageId)
        {
            repository.Delete<Package>(packageId);
            return true;
        }

        public bool TrackPackage(int packageId)
        {
            var dbPackage = repository.GetById<Package>(packageId);
            if (!dbPackage.Tracking)
            {
                dbPackage.Tracking = true;
                repository.Update(dbPackage);
            }
            return true;
        }
    }
}
