﻿using Repository.Models;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IClientService" in both code and config file together.
    [ServiceContract]
    public interface IClientService
    {
        [OperationContract]
        AuthenticationStatus Login(LoginInfo user);

        [OperationContract]
        bool Register(RegisterInformation user);

        [OperationContract]
        List<PackageInformation> GetSentPackages(int clientId, string searchString);

        [OperationContract]
        List<PackageInformation> GetReceivedPackages(int clientId, string searchString);

        [OperationContract]
        PackageStatus GetPackageStatus(int packageId);
    }
}
