﻿using Repository.Models;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services
{
    public static class MappingService
    {
        internal static PackageInformation MapToPackageInformation(Package package)
        {
            if (package == null)
            {
                return null;
            }

            var packageInfo = new PackageInformation();
            packageInfo.PackageID = package.PackageID;
            packageInfo.Name = package.Name;
            packageInfo.Description = package.Description;
            packageInfo.SenderID = package.SenderID;
            packageInfo.SenderName = package.Sender.Name;
            packageInfo.ReceiverID = package.ReceiverID;
            packageInfo.ReceiverName = package.Receiver.Name;
            packageInfo.Tracking = package.Tracking;
            packageInfo.SenderCity = package.SenderCity;
            packageInfo.DestinationCity = package.DestinationCity;

            return packageInfo;
        }

        internal static List<PackageInformation> MapToPackageInformationList(IQueryable<Package> packages)
        {
            return packages.AsEnumerable().Select(p => MapToPackageInformation(p)).ToList();
        }

        internal static List<RoutePointInfo> MapToRoutePointInfoList(IQueryable<RoutePoint> routePoints)
        {
            return routePoints.AsEnumerable().Select(rp => MapToRoutePointInfo(rp)).ToList();
        }

        internal static RoutePoint MapToRoutePont(RoutePointInfo rpi)
        {
            if (rpi == null)
            {
                return null;
            }
            var routePoint = new RoutePoint();
            routePoint.RoutePointID = rpi.RoutePointID;
            routePoint.Time = rpi.Time;
            routePoint.City = rpi.City;
            routePoint.PackageID = rpi.PackageID;
            return routePoint;
        }

        internal static User MapToUser(RegisterInformation registerInfo)
        {
            if (registerInfo == null)
            {
                return null;
            }
            var user = new User()
            {
                Name = registerInfo.Name,
                Username = registerInfo.Username,
                City = registerInfo.City,
                Email = registerInfo.Email,
                Password = registerInfo.Password
            };
            return user;
        }

        internal static Package MapToPackage(PackageInformation packageInfo)
        {
            if (packageInfo == null)
            {
                return null;
            }
            var package = new Package()
            {
                Name = packageInfo.Name,
                Description = packageInfo.Description,
                Tracking = packageInfo.Tracking,
                SenderID = packageInfo.SenderID,
                ReceiverID = packageInfo.ReceiverID
            };
            return package;
        }

        private static RoutePointInfo MapToRoutePointInfo(RoutePoint rp)
        {
            if (rp == null)
            {
                return null;
            }
            var routePointInfo = new RoutePointInfo();
            routePointInfo.RoutePointID = rp.RoutePointID;
            routePointInfo.Time = rp.Time;
            routePointInfo.City = rp.City;
            routePointInfo.PackageID = rp.PackageID;
            return routePointInfo;
        }

        internal static ClientInformation MapToUserInformation(User user)
        {
            if (user == null)
            {
                return null;
            }
            var userInfo = new ClientInformation() { UserID = user.UserID, Name = user.Name };
            return userInfo;
        }

        internal static List<ClientInformation> MapToUserInformationList(List<User> users)
        {
            if (users == null)
            {
                return null;
            }
            return users.AsEnumerable().Select(u => MapToUserInformation(u)).ToList();
        }
    }
}