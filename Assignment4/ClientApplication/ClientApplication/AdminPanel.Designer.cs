﻿namespace ClientApplication
{
    partial class AdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.packagesDataGrid = new System.Windows.Forms.DataGridView();
            this.removeBtn = new System.Windows.Forms.Button();
            this.trackBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nameTb = new System.Windows.Forms.TextBox();
            this.descriptionTb = new System.Windows.Forms.TextBox();
            this.senderCb = new System.Windows.Forms.ComboBox();
            this.receiverCb = new System.Windows.Forms.ComboBox();
            this.addPackageBtn = new System.Windows.Forms.Button();
            this.trackingCheck = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cityTb = new System.Windows.Forms.TextBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.addRoutePointBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.packagesDataGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // packagesDataGrid
            // 
            this.packagesDataGrid.AllowUserToAddRows = false;
            this.packagesDataGrid.AllowUserToDeleteRows = false;
            this.packagesDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.packagesDataGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.packagesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.packagesDataGrid.Location = new System.Drawing.Point(44, 25);
            this.packagesDataGrid.Name = "packagesDataGrid";
            this.packagesDataGrid.ReadOnly = true;
            this.packagesDataGrid.Size = new System.Drawing.Size(898, 264);
            this.packagesDataGrid.TabIndex = 0;
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(206, 307);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(106, 23);
            this.removeBtn.TabIndex = 1;
            this.removeBtn.Text = "Remove selected";
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // trackBtn
            // 
            this.trackBtn.Location = new System.Drawing.Point(72, 307);
            this.trackBtn.Name = "trackBtn";
            this.trackBtn.Size = new System.Drawing.Size(106, 23);
            this.trackBtn.TabIndex = 1;
            this.trackBtn.Text = "Track selected";
            this.trackBtn.UseVisualStyleBackColor = true;
            this.trackBtn.Click += new System.EventHandler(this.trackBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 360);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Add package";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 391);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 423);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 391);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Sender";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 456);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Tracking";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(280, 423);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Receiver";
            // 
            // nameTb
            // 
            this.nameTb.Location = new System.Drawing.Point(120, 391);
            this.nameTb.Name = "nameTb";
            this.nameTb.Size = new System.Drawing.Size(121, 20);
            this.nameTb.TabIndex = 4;
            // 
            // descriptionTb
            // 
            this.descriptionTb.Location = new System.Drawing.Point(120, 420);
            this.descriptionTb.Name = "descriptionTb";
            this.descriptionTb.Size = new System.Drawing.Size(121, 20);
            this.descriptionTb.TabIndex = 4;
            // 
            // senderCb
            // 
            this.senderCb.FormattingEnabled = true;
            this.senderCb.Location = new System.Drawing.Point(345, 388);
            this.senderCb.Name = "senderCb";
            this.senderCb.Size = new System.Drawing.Size(121, 21);
            this.senderCb.TabIndex = 5;
            // 
            // receiverCb
            // 
            this.receiverCb.FormattingEnabled = true;
            this.receiverCb.Location = new System.Drawing.Point(345, 420);
            this.receiverCb.Name = "receiverCb";
            this.receiverCb.Size = new System.Drawing.Size(121, 21);
            this.receiverCb.TabIndex = 5;
            // 
            // addPackageBtn
            // 
            this.addPackageBtn.Location = new System.Drawing.Point(283, 456);
            this.addPackageBtn.Name = "addPackageBtn";
            this.addPackageBtn.Size = new System.Drawing.Size(88, 23);
            this.addPackageBtn.TabIndex = 6;
            this.addPackageBtn.Text = "Add package";
            this.addPackageBtn.UseVisualStyleBackColor = true;
            this.addPackageBtn.Click += new System.EventHandler(this.addPackageBtn_Click);
            // 
            // trackingCheck
            // 
            this.trackingCheck.AutoSize = true;
            this.trackingCheck.Location = new System.Drawing.Point(120, 456);
            this.trackingCheck.Name = "trackingCheck";
            this.trackingCheck.Size = new System.Drawing.Size(15, 14);
            this.trackingCheck.TabIndex = 7;
            this.trackingCheck.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.addRoutePointBtn);
            this.groupBox1.Controls.Add(this.dateTimePicker);
            this.groupBox1.Controls.Add(this.cityTb);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(505, 307);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(437, 227);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add route point to selected package";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "City";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Time";
            // 
            // cityTb
            // 
            this.cityTb.Location = new System.Drawing.Point(76, 50);
            this.cityTb.Name = "cityTb";
            this.cityTb.Size = new System.Drawing.Size(200, 20);
            this.cityTb.TabIndex = 1;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(76, 78);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker.TabIndex = 2;
            // 
            // addRoutePointBtn
            // 
            this.addRoutePointBtn.Location = new System.Drawing.Point(76, 116);
            this.addRoutePointBtn.Name = "addRoutePointBtn";
            this.addRoutePointBtn.Size = new System.Drawing.Size(92, 23);
            this.addRoutePointBtn.TabIndex = 3;
            this.addRoutePointBtn.Text = "Add route point";
            this.addRoutePointBtn.UseVisualStyleBackColor = true;
            this.addRoutePointBtn.Click += new System.EventHandler(this.addRoutePointBtn_Click);
            // 
            // AdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 546);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.trackingCheck);
            this.Controls.Add(this.addPackageBtn);
            this.Controls.Add(this.receiverCb);
            this.Controls.Add(this.senderCb);
            this.Controls.Add(this.descriptionTb);
            this.Controls.Add(this.nameTb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackBtn);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.packagesDataGrid);
            this.Name = "AdminPanel";
            this.Text = "AdminPanel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminPanel_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.packagesDataGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView packagesDataGrid;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.Button trackBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox nameTb;
        private System.Windows.Forms.TextBox descriptionTb;
        private System.Windows.Forms.ComboBox senderCb;
        private System.Windows.Forms.ComboBox receiverCb;
        private System.Windows.Forms.Button addPackageBtn;
        private System.Windows.Forms.CheckBox trackingCheck;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox cityTb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Button addRoutePointBtn;
    }
}