﻿using ClientApplication.ClientWebService;
using ClientApplication.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class ClientPanel : Form
    {
        private Form _previousWindow;
        private AuthenticationStatus _loggedUser;
        private ClientServiceClient _clientService;
        private List<PackageInformation> packages;

        public ClientPanel(Form previousWindow)
        {
            InitializeComponent();
            _previousWindow = previousWindow;
            _loggedUser = LoginState.GetLoggedUser();
            _clientService = new ClientServiceClient();
            InitializePackagesDataGridColumns();
            InitializeRouteDataGridColumns();
        }

        private void ClientPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoginState.LogoutUser();
            _previousWindow.Show();
        }

        private void searchSentBtn_Click(object sender, EventArgs e)
        {
            packagesDataGrid.DataSource = null;
            packages = _clientService.GetSentPackages(_loggedUser.UserID, searchTb.Text).ToList();
            var bindingList = new BindingList<PackageInformation>(packages);
            var source = new BindingSource(bindingList, null);
            packagesDataGrid.DataSource = source;
        }

        private void searchReceivedBtn_Click(object sender, EventArgs e)
        {
            packagesDataGrid.DataSource = null;
            packages = _clientService.GetReceivedPackages(_loggedUser.UserID, searchTb.Text).ToList();
            var bindingList = new BindingList<PackageInformation>(packages);
            var source = new BindingSource(bindingList, null);
            packagesDataGrid.DataSource = source;
        }

        private void checkStatusBtn_Click(object sender, EventArgs e)
        {
            routeDataGrid.DataSource = null;
            if (packagesDataGrid.SelectedRows.Count > 0)
            {
                var packageIndex = packagesDataGrid.SelectedRows[0].Index;
                var selectedPackage = packages.ElementAt(packageIndex);
                var packageStatus = _clientService.GetPackageStatus(selectedPackage.PackageID);
                if (packageStatus.RoutePoints.Length > 0)
                {
                    var bindingList = new BindingList<RoutePointInfo>(packageStatus.RoutePoints.ToList());
                    var source = new BindingSource(bindingList, null);
                    routeDataGrid.DataSource = source;
                }
            }
        }

        private void InitializePackagesDataGridColumns()
        {
            packagesDataGrid.AutoGenerateColumns = false;
            packagesDataGrid.ColumnCount = 7;

            packagesDataGrid.Columns[0].Name = "Name";
            packagesDataGrid.Columns[0].DataPropertyName = "Name";
            packagesDataGrid.Columns[1].Name = "Description";
            packagesDataGrid.Columns[1].DataPropertyName = "Description";
            packagesDataGrid.Columns[2].Name = "Tracking";
            packagesDataGrid.Columns[2].DataPropertyName = "Tracking";
            packagesDataGrid.Columns[3].Name = "Sender";
            packagesDataGrid.Columns[3].DataPropertyName = "SenderName";
            packagesDataGrid.Columns[4].Name = "Sender city";
            packagesDataGrid.Columns[4].DataPropertyName = "SenderCity";
            packagesDataGrid.Columns[5].Name = "Receiver";
            packagesDataGrid.Columns[5].DataPropertyName = "ReceiverName";
            packagesDataGrid.Columns[6].Name = "Destination city";
            packagesDataGrid.Columns[6].DataPropertyName = "DestinationCity";
        }

        private void InitializeRouteDataGridColumns()
        {
            routeDataGrid.AutoGenerateColumns = false;
            routeDataGrid.ColumnCount = 2;

            routeDataGrid.Columns[0].Name = "City";
            routeDataGrid.Columns[0].DataPropertyName = "City";
            routeDataGrid.Columns[1].Name = "Time";
            routeDataGrid.Columns[1].DataPropertyName = "Time";
        }        
    }
}
