﻿using ClientApplication.ClientWebService;
using ClientApplication.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            usernameTb.Text = "user3";
            passwordTb.Text = "password";
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            ClientServiceClient service = new ClientServiceClient();
            LoginInfo user = new LoginInfo() { Username = usernameTb.Text, Password = passwordTb.Text };
            var status = service.Login(user);
            switch (status.Role)
            {
                case AuthenticationRoles.Admin:
                    LoginState.SetLoggedUser(status);
                    var adminPanel = new AdminPanel(this);
                    this.Hide();
                    adminPanel.Show();
                    break;
                case AuthenticationRoles.Client:
                    LoginState.SetLoggedUser(status);
                    var clientPanel = new ClientPanel(this);
                    this.Hide();
                    clientPanel.Show();
                    break;
                case AuthenticationRoles.NotAuthenticated:
                    errorLbl.Text = "Invalid username or password";
                    break;
            }
        }

        private void registerBtn_Click(object sender, EventArgs e)
        {
            var registerPanel = new Register(this);
            this.Hide();
            registerPanel.Show();
        }
    }
}
