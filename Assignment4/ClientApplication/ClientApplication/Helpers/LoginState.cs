﻿using ClientApplication.ClientWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApplication.Helpers
{
    public static class LoginState
    {
        private static AuthenticationStatus loggedUser { get; set; }

        public static void SetLoggedUser(AuthenticationStatus status)
        {
            loggedUser = status;
        }

        public static void LogoutUser()
        {
            loggedUser = null;
        }

        public static AuthenticationStatus GetLoggedUser()
        {
            return loggedUser;
        }
    }
}
