﻿namespace ClientApplication
{
    partial class ClientPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.packageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.packagesDataGrid = new System.Windows.Forms.DataGridView();
            this.searchTb = new System.Windows.Forms.TextBox();
            this.searchSentBtn = new System.Windows.Forms.Button();
            this.searcheReceivedBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.checkStatusBtn = new System.Windows.Forms.Button();
            this.routeDataGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.packageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packagesDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.routeDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // packageBindingSource
            // 
            this.packageBindingSource.DataSource = typeof(ClientApplication.ClientWebService.PackageInformation);
            // 
            // packagesDataGrid
            // 
            this.packagesDataGrid.AllowUserToAddRows = false;
            this.packagesDataGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.packagesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.packagesDataGrid.Location = new System.Drawing.Point(224, 12);
            this.packagesDataGrid.Name = "packagesDataGrid";
            this.packagesDataGrid.ReadOnly = true;
            this.packagesDataGrid.Size = new System.Drawing.Size(743, 257);
            this.packagesDataGrid.TabIndex = 0;
            // 
            // searchTb
            // 
            this.searchTb.Location = new System.Drawing.Point(12, 35);
            this.searchTb.Name = "searchTb";
            this.searchTb.Size = new System.Drawing.Size(191, 20);
            this.searchTb.TabIndex = 1;
            // 
            // searchSentBtn
            // 
            this.searchSentBtn.Location = new System.Drawing.Point(34, 61);
            this.searchSentBtn.Name = "searchSentBtn";
            this.searchSentBtn.Size = new System.Drawing.Size(154, 23);
            this.searchSentBtn.TabIndex = 2;
            this.searchSentBtn.Text = "Search sent packages";
            this.searchSentBtn.UseVisualStyleBackColor = true;
            this.searchSentBtn.Click += new System.EventHandler(this.searchSentBtn_Click);
            // 
            // searcheReceivedBtn
            // 
            this.searcheReceivedBtn.Location = new System.Drawing.Point(34, 90);
            this.searcheReceivedBtn.Name = "searcheReceivedBtn";
            this.searcheReceivedBtn.Size = new System.Drawing.Size(154, 23);
            this.searcheReceivedBtn.TabIndex = 3;
            this.searcheReceivedBtn.Text = "Search received packages";
            this.searcheReceivedBtn.UseVisualStyleBackColor = true;
            this.searcheReceivedBtn.Click += new System.EventHandler(this.searchReceivedBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label1.Location = new System.Drawing.Point(221, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(376, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Package status (select a package and click \'Check status\')";
            // 
            // checkStatusBtn
            // 
            this.checkStatusBtn.Location = new System.Drawing.Point(224, 301);
            this.checkStatusBtn.Name = "checkStatusBtn";
            this.checkStatusBtn.Size = new System.Drawing.Size(94, 25);
            this.checkStatusBtn.TabIndex = 5;
            this.checkStatusBtn.Text = "Check status";
            this.checkStatusBtn.UseVisualStyleBackColor = true;
            this.checkStatusBtn.Click += new System.EventHandler(this.checkStatusBtn_Click);
            // 
            // routeDataGrid
            // 
            this.routeDataGrid.AllowUserToAddRows = false;
            this.routeDataGrid.AllowUserToDeleteRows = false;
            this.routeDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.routeDataGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.routeDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.routeDataGrid.Location = new System.Drawing.Point(224, 333);
            this.routeDataGrid.Name = "routeDataGrid";
            this.routeDataGrid.ReadOnly = true;
            this.routeDataGrid.Size = new System.Drawing.Size(373, 150);
            this.routeDataGrid.TabIndex = 6;
            // 
            // ClientPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 546);
            this.Controls.Add(this.routeDataGrid);
            this.Controls.Add(this.checkStatusBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searcheReceivedBtn);
            this.Controls.Add(this.searchSentBtn);
            this.Controls.Add(this.searchTb);
            this.Controls.Add(this.packagesDataGrid);
            this.Name = "ClientPanel";
            this.Text = "ClientPanel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ClientPanel_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.packageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packagesDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.routeDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource packageBindingSource;
        private System.Windows.Forms.DataGridView packagesDataGrid;
        private System.Windows.Forms.TextBox searchTb;
        private System.Windows.Forms.Button searchSentBtn;
        private System.Windows.Forms.Button searcheReceivedBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button checkStatusBtn;
        private System.Windows.Forms.DataGridView routeDataGrid;
    }
}