﻿using ClientApplication.AdminWebService;
using ClientApplication.ClientWebService;
using ClientApplication.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class AdminPanel : Form
    {
        private Form _previousWindow;
        private AuthenticationStatus _loggedUser;
        private AdminServiceClient _adminService;
        private List<AdminWebService.PackageInformation> packages;

        public AdminPanel(Form previousWindow)
        {
            InitializeComponent();
            _previousWindow = previousWindow;
            _loggedUser = LoginState.GetLoggedUser();
            _adminService = new AdminServiceClient();
            InitializePackagesDataGridColumns();
            GetPackages();
            InitializeComboBoxes();
        }

        private void AdminPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoginState.LogoutUser();
            _previousWindow.Show();
        }

        private void GetPackages()
        {
            packagesDataGrid.DataSource = null;
            packages = _adminService.GetAllPackages(_loggedUser.UserID).ToList();
            var bindingList = new BindingList<AdminWebService.PackageInformation>(packages);
            var source = new BindingSource(bindingList, null);
            packagesDataGrid.DataSource = source;
        }

        private void InitializePackagesDataGridColumns()
        {
            packagesDataGrid.AutoGenerateColumns = false;
            packagesDataGrid.ColumnCount = 7;

            packagesDataGrid.Columns[0].Name = "Name";
            packagesDataGrid.Columns[0].DataPropertyName = "Name";
            packagesDataGrid.Columns[1].Name = "Description";
            packagesDataGrid.Columns[1].DataPropertyName = "Description";
            packagesDataGrid.Columns[2].Name = "Tracking";
            packagesDataGrid.Columns[2].DataPropertyName = "Tracking";
            packagesDataGrid.Columns[3].Name = "Sender";
            packagesDataGrid.Columns[3].DataPropertyName = "SenderName";
            packagesDataGrid.Columns[4].Name = "Sender city";
            packagesDataGrid.Columns[4].DataPropertyName = "SenderCity";
            packagesDataGrid.Columns[5].Name = "Receiver";
            packagesDataGrid.Columns[5].DataPropertyName = "ReceiverName";
            packagesDataGrid.Columns[6].Name = "Destination city";
            packagesDataGrid.Columns[6].DataPropertyName = "DestinationCity";
        }

        private void InitializeComboBoxes()
        {
            var clients = _adminService.GetClients().ToList();
            if (clients != null)
            {
                senderCb.DataSource = clients;
                senderCb.DisplayMember = "Name";
                receiverCb.BindingContext = new BindingContext();
                receiverCb.DataSource = clients;
                receiverCb.DisplayMember = "Name";
            }
        }

        private void RefreshDataGrid()
        {
            packagesDataGrid.DataSource = null;
            var bindingList = new BindingList<AdminWebService.PackageInformation>(packages);
            var source = new BindingSource(bindingList, null);
            packagesDataGrid.DataSource = source;
        }

        private void trackBtn_Click(object sender, EventArgs e)
        {
            var selectedPackage = GetSelectedPackage();
            if (selectedPackage != null)
            {
                var trackStatus = _adminService.TrackPackage(selectedPackage.PackageID);
                if (trackStatus)
                {
                    selectedPackage.Tracking = true;
                    RefreshDataGrid();
                }
            }
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            var selectedPackage = GetSelectedPackage();
            if (selectedPackage != null)
            {
                var status = _adminService.DeletePackage(selectedPackage.PackageID);
                if (status)
                {
                    packages.Remove(selectedPackage);
                    RefreshDataGrid();
                }
            }
        }

        private AdminWebService.PackageInformation GetSelectedPackage()
        {
            if (packagesDataGrid.SelectedRows.Count > 0)
            {
                var packageIndex = packagesDataGrid.SelectedRows[0].Index;
                var selectedPackage = packages.ElementAt(packageIndex);
                return selectedPackage;
            }
            else
            {
                return null;
            }
        }

        private void addPackageBtn_Click(object sender, EventArgs e)
        {
            var insertedPackage = new AdminWebService.PackageInformation()
            {
                Name = nameTb.Text,
                Description = descriptionTb.Text,
                Tracking = trackingCheck.Checked,
                SenderID = ((ClientInformation)senderCb.SelectedValue).UserID,
                ReceiverID = ((ClientInformation)receiverCb.SelectedValue).UserID
            };
            _adminService.AddPackage(insertedPackage);
            GetPackages();
        }

        private void addRoutePointBtn_Click(object sender, EventArgs e)
        {
            var selectedPackage = GetSelectedPackage();
            if (selectedPackage.Tracking)
            {
                var routePointInfo = new AdminWebService.RoutePointInfo()
                {
                    PackageID = selectedPackage.PackageID,
                    City = cityTb.Text,
                    Time = dateTimePicker.Value
                };
                if (_adminService.AddPackageRoutePoint(routePointInfo))
                {
                    MessageBox.Show("Route point added");
                }
            }
            else
            {
                MessageBox.Show("The package is not tracked");
            }
        }
    }
}
