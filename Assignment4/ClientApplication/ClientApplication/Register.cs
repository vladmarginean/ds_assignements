﻿using ClientApplication.ClientWebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class Register : Form
    {
        private Form _previousWindow;
        private ClientServiceClient _clientService;

        public Register(Form previousWindow)
        {
            InitializeComponent();
            _previousWindow = previousWindow;
            _clientService = new ClientServiceClient();
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            _previousWindow.Show();
        }

        private void registerBtn_Click(object sender, EventArgs e)
        {
            var registerInfo = new RegisterInformation()
            {
                Name = nameTb.Text,
                Username = usernameTb.Text,
                Email = emailTb.Text,
                City = cityTb.Text,
                Password = passwordTb.Text
            };
            if (_clientService.Register(registerInfo))
            {
                MessageBox.Show("Registered!");
            }            
        }
    }
}
