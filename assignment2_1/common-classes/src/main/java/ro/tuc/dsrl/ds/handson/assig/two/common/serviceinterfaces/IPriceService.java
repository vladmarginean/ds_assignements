package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

/**
 * Created by Vlad on 08.11.2016.
 */
public interface IPriceService {

    double computeSellingPrice(Car c);
}
