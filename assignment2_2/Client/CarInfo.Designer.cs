﻿namespace Client
{
    partial class CarInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.yearTextBox = new System.Windows.Forms.TextBox();
            this.engineTextBox = new System.Windows.Forms.TextBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.taxButton = new System.Windows.Forms.Button();
            this.priceButton = new System.Windows.Forms.Button();
            this.priceResult = new System.Windows.Forms.TextBox();
            this.taxResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // yearTextBox
            // 
            this.yearTextBox.Location = new System.Drawing.Point(149, 120);
            this.yearTextBox.Name = "yearTextBox";
            this.yearTextBox.Size = new System.Drawing.Size(100, 20);
            this.yearTextBox.TabIndex = 0;
            // 
            // engineTextBox
            // 
            this.engineTextBox.Location = new System.Drawing.Point(149, 82);
            this.engineTextBox.Name = "engineTextBox";
            this.engineTextBox.Size = new System.Drawing.Size(100, 20);
            this.engineTextBox.TabIndex = 0;
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(149, 157);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(100, 20);
            this.priceTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Engine capacity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(49, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Enter the car information";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(114, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(112, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Price";
            // 
            // taxButton
            // 
            this.taxButton.Location = new System.Drawing.Point(53, 195);
            this.taxButton.Name = "taxButton";
            this.taxButton.Size = new System.Drawing.Size(85, 23);
            this.taxButton.TabIndex = 3;
            this.taxButton.Text = "Calculate tax";
            this.taxButton.UseVisualStyleBackColor = true;
            this.taxButton.Click += new System.EventHandler(this.taxButton_Click);
            // 
            // priceButton
            // 
            this.priceButton.Location = new System.Drawing.Point(53, 226);
            this.priceButton.Name = "priceButton";
            this.priceButton.Size = new System.Drawing.Size(85, 23);
            this.priceButton.TabIndex = 3;
            this.priceButton.Text = "Calculate price";
            this.priceButton.UseVisualStyleBackColor = true;
            this.priceButton.Click += new System.EventHandler(this.priceButton_Click);
            // 
            // priceResult
            // 
            this.priceResult.Location = new System.Drawing.Point(149, 226);
            this.priceResult.Name = "priceResult";
            this.priceResult.Size = new System.Drawing.Size(100, 20);
            this.priceResult.TabIndex = 4;
            // 
            // taxResult
            // 
            this.taxResult.Location = new System.Drawing.Point(149, 197);
            this.taxResult.Name = "taxResult";
            this.taxResult.Size = new System.Drawing.Size(100, 20);
            this.taxResult.TabIndex = 4;
            // 
            // CarInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 268);
            this.Controls.Add(this.taxResult);
            this.Controls.Add(this.priceResult);
            this.Controls.Add(this.priceButton);
            this.Controls.Add(this.taxButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.engineTextBox);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(this.yearTextBox);
            this.Name = "CarInfo";
            this.Text = "CarInfo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox yearTextBox;
        private System.Windows.Forms.TextBox engineTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button taxButton;
        private System.Windows.Forms.Button priceButton;
        private System.Windows.Forms.TextBox priceResult;
        private System.Windows.Forms.TextBox taxResult;
    }
}