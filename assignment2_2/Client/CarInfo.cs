﻿using Common;
using Remotable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class CarInfo : Form
    {
        private TaxService taxService;
        private PriceService priceService;
        public CarInfo()
        {
            InitializeComponent();
            TcpChannel chan = new TcpChannel();
            ChannelServices.RegisterChannel(chan);
            // Create an instance of the remote object
            taxService = (TaxService)Activator.GetObject(
                typeof(TaxService), "tcp://localhost:8080/TaxService");
            priceService = (PriceService)Activator.GetObject(
                typeof(PriceService), "tcp://localhost:8080/PriceService");
        }

        private void priceButton_Click(object sender, EventArgs e)
        {
            Car c = buildCarObject();
            if (c != null)
            {
                try
                {
                    var price = priceService.ComputePrice(c);
                    priceResult.Text = price.ToString();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Error computing tax: " + ex.Message);
                }
            }
        }

        private void taxButton_Click(object sender, EventArgs e)
        {
            Car c = buildCarObject();
            if (c != null)
            {
                try
                {
                    var tax = taxService.ComputeTax(c);
                    taxResult.Text = tax.ToString();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Error computing selling price: " + ex.Message);
                }
            }
        }

        private Car buildCarObject()
        {
            Car c = new Car();
            try
            {
                c.EngineSize = Convert.ToInt32(engineTextBox.Text);
                c.Price = Convert.ToDouble(priceTextBox.Text);
                c.Year = Convert.ToInt32(yearTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Input values not valid");
                return null;
            }
            return c;
        }
    }
}
