﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remotable
{
    public class TaxService : MarshalByRefObject
    {
        public TaxService() { }

        public double ComputeTax(Car c)
        {
            int sum = 0;
            if (c.EngineSize <= 1600)
            {
                sum = 8;
            }
            else if (c.EngineSize <= 2000)
            {
                sum = 18;
            }
            else if (c.EngineSize <= 2600)
            {
                sum = 72;
            }
            else if (c.EngineSize <= 3000)
            {
                sum = 144;
            }
            else
            {
                sum = 290;
            }
            return Math.Round((double)c.EngineSize / 200, 2) * sum;
        }
    }
}
