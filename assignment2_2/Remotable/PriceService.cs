﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remotable
{
    public class PriceService : MarshalByRefObject
    {
        public PriceService() { }

        public double ComputePrice(Car c)
        {
            return c.Price - Math.Round(c.Price / 7, 2) * (2015 - c.Year);
        }
    }
}
