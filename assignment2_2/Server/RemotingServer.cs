﻿using Remotable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class RemotingServer : Form
    {
        private PriceService priceService;
        private TaxService taxService;
        public RemotingServer()
        {
            InitializeComponent();
            priceService = new PriceService();
            taxService = new TaxService();
            
            TcpChannel channel = new TcpChannel(8080);
            ChannelServices.RegisterChannel(channel);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(TaxService), "TaxService",
                                                               WellKnownObjectMode.Singleton);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(PriceService), "PriceService",
                                                               WellKnownObjectMode.Singleton);
            //RemotingServer.Cache.Attach(this);
        }
    }
}
